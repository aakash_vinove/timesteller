const express = require('express');
const path = require('path');
const favicon = require('serve-favicon');
const logger = require('morgan');
const cookieParser = require('cookie-parser');

const bodyParser = require('body-parser');
const hbs = require('hbs');

// const config = require('./server/config');
const helmet = require('helmet');



// Setup Database Connection



const app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'html');
app.engine('html', hbs.__express);

app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(helmet());
app.use(logger('dev'));
app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ extended: true, limit: '50mb' }));
app.use(cookieParser());
app.set('trust proxy', 1);
// app.use(session({
//   name: 'session',
//   secret: config.sessionSecret,
//   resave: false,
//   saveUninitialized: false,
//   secure: true,
//   http: true,
// }));
app.use(express.static(path.join(__dirname, 'public')));



// pass the authenticaion checker middleware


// Authentication API
// app.use('/auth', auth);

// Application APIs

// Serving Font Awsome and Icons Files
app.use('/font-awesome', express.static(path.join(__dirname, '/node_modules/font-awesome/')));
app.use('/simple-line-icons', express.static(path.join(__dirname, '/node_modules/simple-line-icons/')));

// catch 404 and forward to error handler
app.use((req, res) => {
  res.render('index', { title: 'Express' });
});

// error handler
app.use((err, req, res) => {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
