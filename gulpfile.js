"use strict";

const gulp = require("gulp"),
  concat = require("gulp-concat"),
  cleanCSS = require("gulp-clean-css"),
  livereload = require("gulp-livereload"),
  nodemon = require("gulp-nodemon"),
  notify = require("gulp-notify"),
  sass = require("gulp-sass"),
  sourcemaps = require("gulp-sourcemaps"),
  uglify = require("gulp-uglify"),
  run = require('gulp-run'),
  gutil = require('gulp-util'),
  webpack = require("webpack"),
  webpackStream = require("webpack-stream"),
  webpackConfig = require("./webpack.config.js"),
  browserSync = require('browser-sync').create();

// Variables
const webpackOptions = {
  watch: true,
  output: {
    filename: "bundle.js"
  },
  module: {
    loaders: [
      {
        test: /\.js(x)?$/,
        loader: "babel-loader",
        query: {
          presets: ["env", "react"],
          plugins: ["transform-decorators"]
        },
        exclude: /node_modules/,
      },
      {
        test: /\.css$/,
        use: [
          {
            loader: 'style-loader'
          },
          {
            loader: 'css-loader'
          },
          {
            loader: 'postcss-loader',
            options: {
              plugins: () => [require('autoprefixer')]
            }
          }
        ]
      }
    ]
  },
  resolve: {
		alias: {
			jquery: "jquery-2.0.3",
			"jquery-ui": "jquery-ui-1.10.3",
			"jquery-ui-1.10.3$": "jquery-ui-1.10.3/ui/jquery-ui.js",
		}
	},
	plugins: [
		new webpack.ProvidePlugin({
			jQuery: "jquery",
			$: "jquery"
		}),
	]
};

// Removed the actual task for bevity (it's the same), but left the confi for learning purposes.
const webpackProductionOptions = {
  output: {
    filename: "app.production.js"
  },
  module: {
    rules: [
      {
        exclude: /node_modules/
      }
    ],
    loaders: [
      {
        test: "app.js",
        loader: "babel-loader",
        query: {
          presets: ["es2015"]
        },
        exclude: /node_modules/,
      }
    ]
  }
};

const nodemonOptions = {
  script: "bin/www",
  // exec: "npm run babel-node",
  ext: "js",
  ignore: ["node_modules/", "public/", "views/", "gulpfile.js", "package.json"]
};

// Task
gulp.task("serve", function() {
  livereload.listen();
  // browserSync.init({
  //   proxy: "localhost:3000"
  // });
  nodemon(nodemonOptions).on("restart", function() {
    gulp
      .src("bin/www")
      .pipe(livereload())
      .pipe(notify("Reloading page, please wait..."));
      // .pipe(run("npm run lint"));
  });
});

gulp.task("packFront", function() {
  return gulp
      .src("views/js/app.js")
      .pipe(sourcemaps.init())
      .pipe(webpackStream(webpackOptions))
      // .pipe(uglify()); Removed from dev
      .pipe(sourcemaps.write())
      .pipe(gulp.dest("public/javascripts"));
});

gulp.task("packCss", function() {
  return gulp
    .src("views/sass/**/*.scss")
    .pipe(sourcemaps.init())
    .pipe(sass().on("error", sass.logError))
    .pipe(cleanCSS())
    .pipe(sourcemaps.write())
    .pipe(concat("bundle.css"))
    .pipe(gulp.dest("public/stylesheets"));
});

gulp.task("build", ["packFront", "packCss", "serve"], function() {
  gulp.watch(["views/js/**/*.js", "views/js/**/*.jsx"], ["packFront"]).on('change', browserSync.reload);
  gulp.watch(["views/sass/**/*.scss"], ["packCss"]);
});

// todo: production build

gulp.task("default", ["build"]);
