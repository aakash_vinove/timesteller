/*eslint-disable */
import React from 'react';
import { BrowserRouter as Router, Route, Redirect, Switch } from 'react-router-dom';

import Login from '../views/js/components/Login';
import AdminHome from '../views/js/components/Dashboard';
import DashLayout from '../views/js/components/Layout';
import Deals from '../views/js/components/Deals';
import usersMangement from '../views/js/components/UsersMangement'
import ForgotPassword from '../views/js/components/ForgotPassword';
import watchManagement from '../views/js/components/WatchManagement'
import Dealstype from '../views/js/components/DealsType';
import Post from '../views/js/components/Post';
import Intention from '../views/js/components/Intention';
import Support from '../views/js/components/Support';
import Setting from '../views/js/components/Setting';
import Redemption from '../views/js/components/Redemption';
import ResetPassword from '../views/js/components/ResetPassword';
import ErrorReport from '../views/js/components/ErrorReport'
const PrivateRoutes = ({ component: Component, ...params }) => (
  <Route
    {...params}
    render={props => (
      localStorage.getItem('token') ? (
        <DashLayout {...props}>
          <Component {...props} {...params} />
        </DashLayout>
      ) : (
        <Redirect to={
          {
            pathname: '/login',
            state: {
              from: props.location,
              message: 'Please Sign in to Continue',
            },
          }
        }
        />
      )
    )}
  />
);

class Routes extends React.Component {
  render() {
    return (
      <Router>
        <Switch>

          <Route path="/" exact component={Login} />
          <Route path="/login" exact component={Login} />
          <Route path="/forgotPassword" exact component={ForgotPassword} />
          <Route path="/post" exact component={Post} />
          <PrivateRoutes path="/admin/dashboard" component={AdminHome} />
          <PrivateRoutes path="/admin/resetpassword"  component={ResetPassword} />
          <PrivateRoutes path="/admin/users" component={usersMangement} />
          <PrivateRoutes path="/admin/watches" component={watchManagement} />
          <PrivateRoutes path="/admin/dealstype" component={Dealstype} />
          <PrivateRoutes path="/admin/intention" component={Intention} />
          <PrivateRoutes path="/admin/redemption" component={Redemption} />
          <PrivateRoutes path="/admin/support" component={Support} />
          <PrivateRoutes path="/admin/setting" component={Setting} />
          <PrivateRoutes path="/admin/errorreport" component={ErrorReport} />
          <Redirect from="/admin" to="/admin/dashboard" />
        </Switch>
      </Router>
    );
  }
}

export default Routes;
