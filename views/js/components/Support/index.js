import React, { Component } from 'react';
import {
  Container, Row, Col, Card, CardHeader, CardBody, FormGroup,
  Form,
  Label,
  Input,
  Button,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Collapse,
} from 'reactstrap';
import { de } from 'date-fns/esm/locale';
import Pagination from 'react-js-pagination';
import { toasterMessage } from '../../utils/toaster';

import proxy from '../../utils/proxy';
import Loader from '../../Common/Loader/Index';
import CustomTable from '../../Common/Table';


export default class index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: '',
      delModal: false,
      replyModal: false,
      queryList: '',
      showLoader: false,
      reply: '',
      message: {
        email: '',
        id: '',
        description: '',
      },
    };
    this.reply = this.reply.bind(this);
    this.toggledel = this.toggledel.bind(this);
    this.toggleReply = this.toggleReply.bind(this);
    this.sendMessage = this.sendMessage.bind(this);
    this.deleteQuery = this.deleteQuery.bind(this);
  }
  componentDidMount() {
    this.getQueries();
  }

  getQueries() {

    const resturantId = '5d4bf25a99ada41ad5aca4f3';
    const resturanid = { resturantId };
    if (localStorage.getItem('token')) {
      console.log('token', localStorage.getItem('token'));
      this.setState({ showLoader: true });
      proxy.call('get', 'help', resturanid, null, localStorage.getItem('token'))
        .then((response) => {
          console.log(response, ';');
          if (
            response
            && (
              response.status === 200
              || response.status === 201
            )
            && response.data
            && response.data.data

          ) {

            this.setState({
              ...this.state,
              queryList: response.data.data,
              showLoader: false,


            });
          }
        })
        .catch((err) => {
          // this.setState({ showLoader: false });
          toasterMessage(
            'error',
            (
              err
              && err.response
              && err.response.data
              && err.response.data.message
            ) || (
              err
              && err.message
            ),
          );
        });
    } else {
      // this.setState({ showLoader: false });
      this.props.history.push('/');
    }


  }
  remove(item) {
    this.setState({ id: item._id });
    this.toggledel();
  }
  toggledel() {
    this.setState(prevState => ({
      delModal: !prevState.delModal,
    }));
  }
  toggleReply() {
    this.setState(prevState => ({
      replyModal: !prevState.replyModal,

    }));
  }
  reply(deal) {
    this.toggleReply();
    this.setState({
      message: {
        email: deal.resturantId.email,
        description: deal.resturantId.description,
      },
    }, () => console.log(this.state.message, 'messss'));

  }
  sendMessage() {
    this.toggleReply();
    const body = {
      message: this.state.reply,
      email: this.state.message.email,
    };
    if (localStorage.getItem('token')) {
      console.log('token', localStorage.getItem('token'));
      this.setState({ showLoader: true });
      proxy.call('post', 'sendReplyToUser', null, body, localStorage.getItem('token'))
        .then((response) => {
          console.log(response, ';');
          if (
            response
            && (
              response.status === 200
              || response.status === 201
            )

          ) {
            toasterMessage(
              'success',
              (
                response
                && response.data

                && response.data.message
              ) || (
                response
                && response.message
              ),
            );
            this.setState({
              ...this.state,

              showLoader: false,


            });
            this.getQueries();
          }
        })
        .catch((err) => {
          // this.setState({ showLoader: false });
          toasterMessage(
            'error',
            (
              err
              && err.response
              && err.response.data
              && err.response.data.message
            ) || (
              err
              && err.message
            ),
          );
        });
    } else {
      // this.setState({ showLoader: false });
      this.props.history.push('/');
    }

  }
  deleteQuery() {
    const id = this.state.id;
    const Id = { id };
    this.toggledel();
    if (localStorage.getItem('token')) {
      console.log('token', localStorage.getItem('token'));
      this.setState({ showLoader: true });
      proxy.call('delete', 'helpDelete', Id, null, localStorage.getItem('token'))
        .then((response) => {
          if (
            response
            && (
              response.status === 200
              || response.status === 201
            )

          ) {
            toasterMessage(
              'success',
              (
                response
                && response.data

                && response.data.message
              ) || (
                response
                && response.message
              ),
            );
            this.setState({
              ...this.state,

              showLoader: false,


            });
            this.getQueries();
          }
        })
        .catch((err) => {
          // this.setState({ showLoader: false });
          toasterMessage(
            'error',
            (
              err
              && err.response
              && err.response.data
              && err.response.data.message
            ) || (
              err
              && err.message
            ),
          );
        });
    } else {
      // this.setState({ showLoader: false });
      this.props.history.push('/');
    }
  }
  tableData() {
    return {
      labels: [

        {
          label: 'Name',
          value: 'name',
          isDate: false,
        },

        {
          label: 'Email',
          value: 'email',
          isDate: false,
        }, {
          label: 'message',
          value: 'description',
          isDate: false,
        }, {
          label: 'Created At',
          value: 'messageTime',
          isDate: true,
        },
        {
          label: 'User Type',
          value: 'userType',
          isDate: false,
        },
        {
          label: 'Action',
          value: 'action',
          isDate: false,
        }],
      content: (
        this.state.queryList
        && this.state
        && this.state.queryList.map(deal => ({
          ...deal.resturantId,
          action: (
            <div style={{ textAlign: 'center' }}>
              <span
                className="action-icon edit-icon"
                onClick={() => { this.reply(deal); }}
              >
                <i className="fa fa-reply" />
              </span>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <span
                className="action-icon remove-icon"
                onClick={() => { this.remove(deal); }}
              >
                <i className="icon-trash" />
              </span>
            </div>
          ),
        }))
      ) || null,
    };
  }
  render() {
    return (
      <div>

        {/* delete modal starts */}
        <Modal
          style={{ width: '25rem' }}
          isOpen={this.state.delModal}
          toggle={this.toggledel}
          className={this.props.className}
        >
          <ModalHeader toggle={this.toggledel}>Delete</ModalHeader>
          <ModalBody>Are you sure want to Delete ?  Once deleted can't be retreived </ModalBody>
          <ModalFooter>
            <Button color="danger" onClick={this.deleteQuery}>
              Delete
            </Button>
            <Button color="secondary" onClick={this.toggledel}>
              Cancel
            </Button>
          </ModalFooter>
        </Modal>
        {/* delet modal ends */}
        {/* reply modal starts */}
        <Modal
          style={{ width: '25rem' }}
          isOpen={this.state.replyModal}
          toggle={this.toggledel}
          className={this.props.className}
        >
          <ModalHeader toggle={this.toggleReply}>Reply</ModalHeader>
          <ModalBody><p>{this.state.message.description}</p> </ModalBody>
          <Form className="mb-3">
            <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
              <Col sm={12}>
                <Input onChange={e => this.setState({ reply: e.target.value })} type="textarea" name="email" id="exampleEmail" placeholder="write your message" />
              </Col>
            </FormGroup>

          </Form>
          <ModalFooter>
            <Button color="danger" onClick={this.sendMessage}>
              Send
            </Button>
            <Button color="secondary" onClick={this.toggleReply}>
              Cancel
            </Button>
          </ModalFooter>
        </Modal>
        {/* reply modal ends */}


        <CustomTable data={this.tableData()} />
        {this.state.showLoader ? <Loader /> : ''}
      </div>
    );
  }
}
