import React, { Component } from 'react';
import {
  Container,
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  Collapse,
  Button,
  Row,
  Col,
  FormGroup,
  Form,
  Label,
  Input,
  Modal,
  ModalFooter,
  ModalHeader,
  ModalBody,

} from 'reactstrap';
import DatePicker, { registerLocale } from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import Moment from 'moment';
// import Moment from 'react-moment';
// import 'moment-timezone';
import Deals from '../Deals';
import CustomTable from '../../Common/Table';
import { toasterMessage } from '../../utils/toaster';
import proxy from '../../utils/proxy';
import Loader from '../../Common/Loader/Index';
// import { getDefaultSettings } from 'http2';


export default class Details extends Component {
  constructor(props) {
    super(props);
    this.state = {
      restaurants: '',
      showLoader: false,
      dealList: [],
      historyModal: false,
      startDate: '',
      endDate: '',
      resturantInfo: [],
      redemptionHistory: [],
      pagination: {
        activePage: 1,
        itemsCountPerPage: 10,
        pageRangeDisplayed: 5,
      },
    };
    this.getDeals = this.getDeals.bind(this);
    this.handleChangeStart = this.handleChangeStart.bind(this);
    this.handleChangeEnd = this.handleChangeEnd.bind(this);
    this.getDetails = this.getDetails.bind(this);
    this.restaurantsInfo = this.restaurantsInfo.bind(this);
    this.unFeatured = this.unFeatured.bind(this);
    this.featured = this.featured.bind(this);
    this.showHistory = this.showHistory.bind(this);
    this.toggleDetail = this.toggleDetail.bind(this);
  }
  componentDidMount() {
    this.getDeals();
    this.getDetails();
  }
  getDeals() {
    const resturantId = this.props.match.params.id;
    const resturanid = { resturantId };
    if (localStorage.getItem('token')) {
      console.log('token', localStorage.getItem('token'));
      this.setState({ showLoader: true });
      proxy.call('get', 'dealListAdmin', resturanid, null, localStorage.getItem('token'))
        .then((response) => {
          console.log(response, ';');
          if (
            response
            && (
              response.status === 200
              || response.status === 201
            )
            && response.data
            && response.data.data

          ) {

            this.setState({
              ...this.state,
              dealList: response.data.data,
              showLoader: false,


            });
          }
        })
        .catch((err) => {
          // this.setState({ showLoader: false });
          toasterMessage(
            'error',
            (
              err
              && err.response
              && err.response.data
              && err.response.data.message
            ) || (
              err
              && err.message
            ),
          );
        });
    } else {
      // this.setState({ showLoader: false });
      this.props.history.push('/');
    }


  }
  getDetails() {
    const data = {};
    const id = this.props.match.params.id;
    const Id = { id };
    if (localStorage.getItem('token')) {
      console.log('token', localStorage.getItem('token'));

      proxy.call('get', 'resturantDetail', Id, null, localStorage.getItem('token'))
        .then((response) => {
          console.log(response, ';resturantDetail');
          if (
            response
            && (
              response.status === 200
              || response.status === 201
            )
            && response.data
            && response.data.data
          ) {
            this.setState({
              ...this.state,
              resturantInfo: response.data.data.map(item => item),
              pagination: {
                ...this.state.pagination,
                totalItemsCount: response.data.pageCount,
              },
            }, () => console.log(this.state.resturantInfo, 'resInfo'));
          }
        })
        .catch((err) => {
          // this.setState({ showLoader: false });
          toasterMessage(
            'error',
            (
              err
              && err.response
              && err.response.data
              && err.response.data.message
            ) || (
              err
              && err.message
            ),
          );
        });
    } else {
      // this.setState({ showLoader: false });
      this.props.history.push('/');
    }
  }
  unFeatured(item) {
    const id = item._id;
    const Id = { id };
    if (localStorage.getItem('token')) {
      console.log('token', localStorage.getItem('token'));

      proxy.call('put', 'unFeatured', Id, null, localStorage.getItem('token'))
        .then((response) => {

          if (
            response
            && (
              response.status === 200
              || response.status === 201
            )


          ) {
            this.getDeals();
          }
        })
        .catch((err) => {
          // this.setState({ showLoader: false });
          toasterMessage(
            'error',
            (
              err
              && err.response
              && err.response.data
              && err.response.data.message
            ) || (
              err
              && err.message
            ),
          );
        });
    } else {
      // this.setState({ showLoader: false });
      this.props.history.push('/');
    }
  }
  featured(item) {
    console.log(item);
    const id = item._id;
    const Id = { id };
    if (localStorage.getItem('token')) {
      console.log('token', localStorage.getItem('token'));

      proxy.call('put', 'featured', Id, null, localStorage.getItem('token'))
        .then((response) => {

          if (
            response
            && (
              response.status === 200
              || response.status === 201
            )


          ) {
            this.getDeals();
          }
        })
        .catch((err) => {
          // this.setState({ showLoader: false });
          toasterMessage(
            'error',
            (
              err
              && err.response
              && err.response.data
              && err.response.data.message
            ) || (
              err
              && err.message
            ),
          );
        });
    } else {
      // this.setState({ showLoader: false });
      this.props.history.push('/');
    }
  }
  handleChangeStart(date) {

    this.setState({
      startDate: date,
    });

  }
  handleChangeEnd(date) {
    this.setState({ endDate: date }, () => console.log(Moment(this.state.startDate).format('YYYY/MM/DD') + '-' + Moment(this.state.endDate).format('YYYY/MM/DD'), 'final sdat'));
  }
  toggleDetail() {
    this.setState(prevState => ({
      historyModal: !prevState.historyModal,
    }));
  }
  showHistory(item) {
    console.log(item, 'itemss');
    this.toggleDetail();
    if (localStorage.getItem('token')) {
      console.log('token', localStorage.getItem('token'));

      proxy.call('get', `redemptionHistory/${item._id}`, null, null, localStorage.getItem('token'))
        .then((response) => {
          console.log(response, 'respoionse');
          if (
            response
            && (
              response.status === 200
              || response.status === 201
            )


          ) {
            this.setState({ redemptionHistory: response.data.data });
            this.getDeals();
          }
        })
        .catch((err) => {
          // this.setState({ showLoader: false });
          toasterMessage(
            'error',
            (
              err
              && err.response
              && err.response.data
              && err.response.data.message
            ) || (
              err
              && err.message
            ),
          );
        });
    } else {
      // this.setState({ showLoader: false });
      this.props.history.push('/');
    }

  }
  tableData() {
    return {
      labels: [

        {
          label: 'Title',
          value: 'dealTitle',
          isDate: false,
        },

        {
          label: 'Expiry Date',
          value: 'dealExpiryDate',
          isDate: true,
        }, {
          label: 'Expiry Time',
          value: 'dealExpiryTime',
          isDate: false,
        }, {
          label: 'Total Coupons',
          value: 'availableCoupon',
          isDate: false,
        },
        {
          label: 'Available Coupon',
          value: 'availableCoupon',
          isDate: false,
        }, {
          label: 'People Limit',
          value: 'peopleLimit',
          isDate: false,
        },
        {
          label: 'Featured',
          value: 'featured',
          isDate: false,
        },
        {
          label: 'Status',
          value: 'Status',
          isDate: false,
        },
        // {
        //   label: 'Redem History',
        //   value: 'history',
        //   isDate: false,
        // },
      ],
      content: (this.state.dealList.map(item => ({
        ...item,
        featured: (
          <div style={{ textAlign: 'center' }}>
            {item.isFeatured !== 'false' ?
              <input onChange={e => this.unFeatured(item, e)} checked type="checkbox" />
              :
              <input onChange={e => this.featured(item, e)} type="checkbox" />}
            <span />
          </div>
        ),
        // history: (
        //   <div style={{ textAlign: 'center' }}>
        //     <span onClick={() => this.showHistory(item)} >

        //       <i className="icon-eye" />
        //     </span>

        //   </div>
        // ),

      }))) || null,
    };
  }
  redemptionTable() {
    return {
      labels: [
        {
          label: 'Customer Name',
          value: 'name',
          isDate: false,
        },

        {
          label: 'No Of People',
          value: 'patrons',
          isDate: true,
        }, {
          label: 'Refrence Number',
          value: 'referenceNumber',
          isDate: false,
        }, {
          label: 'Date',
          value: 'claimedDate',
          isDate: false,
        },
        {
          label: 'Time',
          value: 'claimedTime',
          isDate: false,
        },
      ],
      content: (this.state.redemptionHistory.map(item => ({
        ...item,
        // featured: (
        //   <div style={{ textAlign: 'center' }}>
        //     {item.isFeatured !== 'false' ?
        //       <input onChange={e => this.unFeatured(item, e)} checked type="checkbox" />
        //       :
        //       <input onChange={e => this.featured(item, e)} type="checkbox" />}
        //     <span />
        //   </div>
        // ),
        // history: (
        //   <div style={{ textAlign: 'center' }}>
        //     <span onClick={() => this.showHistory(item)} >

        //       <i className="icon-eye" />
        //     </span>

        //   </div>
        // ),

      }))) || null,
    };
  }

  restaurantsInfo() {
  }

  render() {
    console.log(this.props.match.params.id, 'props');
    return (
      <div>
        {this.state.showLoader ? <Loader /> : ''}
        <Modal
          style={{ width: '105rem' }}
          isOpen={this.state.historyModal}
          toggle={this.toggleDetail}
          className={this.props.className}
        >
          <ModalHeader toggle={this.toggleDetail}>Redeemed History</ModalHeader>
          <ModalBody>
            <div>
              <Card className="shadow">
                <CardBody>
                  <CustomTable data={this.redemptionTable()} />
                </CardBody>
              </Card>
            </div>
          </ModalBody>
          <ModalFooter>
            {/* <Button color="danger" onClick={this.deleteResturant}>
                Block
            </Button> */}
            <Button color="secondary" onClick={this.toggleDetail}>
                  Close
            </Button>
          </ModalFooter>
        </Modal>
        <Card className="bg-light" >
          <CardHeader className="bg-dark text-light">Restaurant details</CardHeader>
          {this.state.resturantInfo.map(item => (
            <CardBody>
              <div className="row">
                <div className="col-lg-2 col-md-6">
                  Name :-
                  <br />
                  {item.ownerID.name}
                </div>
                <div className="col-lg-2 col-md-6">
                  Price Range :-
                  <br />
                  {item.priceCategory}
                </div>
                <div className="col-lg-2 col-md-6">
                  Email :-
                  <br />
                  {item.ownerID.email}
                </div>
                <div className="col-lg-2 col-md-6">
                  Contact Number :-
                  <br />
                  {item.ownerID.contactNumber}
                </div>
              </div>

            </CardBody>))}
        </Card>
        <Card style={{ width: '100%' }} >
          <CardBody>

            <CardTitle className="bg-dark text-light p-2 " >Deals </CardTitle>
            {/* <Form className="mb-5 p-2">
              <Row className="justify-content-center mb-4">
                <Col md="3">
                  <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                    <Label for="restaurantName" className="mr-sm-2">
                      Title
                    </Label>
                    <Input
                      type="text"
                      name="resturantName"
                      id="restaurantName"
                      onChange={(event) => {
                        this.setState({ restaurantName: event.target.value });
                      }}
                    />
                  </FormGroup>
                </Col>
                <Col md="4" >
                  <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                    <Label for="email" className="mr-sm-2">
                      Date
                    </Label>
                    <br />
                    <DatePicker

                      className="p-1"
                      selected={this.state.startDate}

                      startDate={this.state.startDate}
                      // endDate={this.state.endDate}
                      onChange={this.handleChangeStart}
                      dateFormat="dd/MM/yyyy"
                    />

                    <DatePicker
                      className="p-1"
                      selected={this.state.endDate}
                      selectsEnd
                      startDate={this.state.startDate}
                      endDate={this.state.endDate}
                      onChange={this.handleChangeEnd}
                      minDate={this.state.startDate}
                      dateFormat="dd/MM/yyyy"
                    />
                  </FormGroup>
                </Col>
                <Col md="3">
                  <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                    <Label for="contactNumber" className="mr-sm-2">
                      Contact Number
                    </Label>
                    <Input
                      type="text"
                      name="contactNumber"
                      id="contactNumber"
                      onChange={(event) => {
                        this.setState({ contactNumber: event.target.value });
                      }}
                    />
                  </FormGroup>
                </Col>
                <Col md="3">
                  <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                    <Label for="priceCategory" className="mr-sm-2">
                      Status
                    </Label>
                    <Input
                      value={this.state.fields.gender}
                      onChange={(event) => {
                        this.setState({ fields: { ...this.state.fields, gender: event.target.value } }, () => console.log(this.state.fields));
                      }}
                      type="select"
                      name="select"
                      id="exampleSelect"
                    >
                      <option value="Male" >Active </option>
                      <option value="femail" >Expired</option>

                    </Input>
                  </FormGroup>
                </Col>
              </Row>
              <Row className="justify-content-center ">
                <Button color="primary" type="submit">
                  Filter
                </Button>
              </Row>

            </Form> */}

            {/* <Deals id={this.props.match.params.id} /> */}
            <CustomTable data={this.tableData()} />
          </CardBody>

        </Card>
      </div>
    );
  }
}
