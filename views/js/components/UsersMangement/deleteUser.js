/*eslint-disable */
import React, { useState, useEffect } from 'react';
import {
 Button,
 Row,
 Col,
 FormGroup,
 Form,
 Label,
 Input,
 Modal,
 ModalBody,
 ModalFooter,
 ModalHeader,
 TabContent, TabPane, Nav, NavItem, NavLink,
} from 'reactstrap';
const DeleteUser=(props)=>{
return(
 <Modal
              style={{ width: '25rem' }}
              isOpen={props.delModal}
              toggle={props.toggledel}
              className={props.className}
            >
              <ModalHeader toggle={props.toggledel}>Delete</ModalHeader>
              <ModalBody>Are you sure want to Delete</ModalBody>
              <ModalFooter>
                <Button color="danger" style={{ "backgroundColor": "red" }} onClick={props.deleteResturant}>
                  Delete
                </Button>
                <Button color="secondary" onClick={props.toggledel}>
                  Cancel
                </Button>
              </ModalFooter>
            </Modal>

)}
export default DeleteUser