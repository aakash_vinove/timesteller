/*eslint-disable */
import React, { useState, useEffect } from 'react';
import { BASE_URL } from '../../utils/config'
import {
 Button,
 Row,
 Col,
 FormGroup,
 Form,
 Label,
 Input,
 Modal,
 ModalBody,
 ModalFooter,
 ModalHeader,
 TabContent, TabPane, Nav, NavItem, NavLink,
} from 'reactstrap';
const EditUser=(props)=>{
  console.log("edit props",props)

  console.log("profile Image",props&&props.fields&&props.profileImage)
return(
          <Modal
              isOpen={props.modal}
              toggle={props.toggleEdit}
              className={props.className}
              backdrop={props.backdrop}
            >
              <ModalHeader toggle={props.toggleEdit}>User Edit</ModalHeader>
              <ModalBody>
                <Form onSubmit={props.checkValidations}>
                  <Row className="justify-content-center mb-4">
                    <Col md="4">
                      <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                        <Label for="restaurantName" className="mr-sm-2">
                          First Name
                        </Label>
                        <div>

                          <Input
                            value={props.fields.firstName}
                            type="text"
                            name="firstName"
                            id="firstName"
                            onChange={props.onChange}
                          />
                        </div>
                        {
                          props.error
                          && props.error.firstName
                          && (<span className="input-error">{props.error.name}</span>)
                        }
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                        <Label for="restaurantName" className="mr-sm-2">
                          Last Name
                        </Label>
                        <div>

                          <Input
                            value={props.fields.lastName}
                            type="text"
                            name="lastName"
                            id="lastName"
                            onChange={props.onChange}
                          />
                        </div>
                        {
                          props.error
                          && props.error.lastName
                          && (<span className="input-error">{props.error.name}</span>)
                        }
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                        <Label for="email" className="mr-sm-2">
                          Email
                        </Label>
                        <Input
                          value={props.fields.email}
                          type="email"
                          name="email"
                          id="email"
                          disabled
                          onChange={props.onChange}
                        />
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                        <Label for="email" className="mr-sm-2">
                          Country Name
                        </Label>
                        <Input
                          value={props.fields.countryName}
                          type="text"
                          name="countryName"
                          id="countryName"

                          onChange={props.onChange}
                        />
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                        <Label for="email" className="mr-sm-2">
                          Profile Image
                        </Label>

                        <Input
                         
                          type="file"
                          name="profileImage"
                          id="profileImage"
                          onChange={props.editImageChange}
                        />
                        {
                          props.showImage?
                          props&&props.fields&&props.profileImage&&props.profileImage.map((data,index)=>(
                            <div>
                            
                        />
                        <img src={`${BASE_URL}${data}`} alt="Smiley face" height="200" width="200" />
                        </div>
                          ))
                        :

                        props && props.imageShow&& props.imageShow.map((data, index) => (

                    <img id={index} src={data} alt="No Image selected" height="200" width="200" />
                  ))

                      }
                      </FormGroup>
                    </Col>
                  </Row>
                </Form>
              </ModalBody>
              <ModalFooter>
                <Button color="primary" onClick={props.checkValidations} type="submit" >
                  Save
                </Button>
                &nbsp;
                <Button color="secondary" onClick={props.toggleEdit}>
                  Cancel
                </Button>
              </ModalFooter>
            </Modal>
)}
export default EditUser