/*eslint-disable */
import React, { useState, useEffect } from 'react';
import {
 Button,
 Row,
 Col,
 FormGroup,
 Form,
 Label,
 Input,
 Modal,
 ModalBody,
 ModalFooter,
 ModalHeader,
 TabContent, TabPane, Nav, NavItem, NavLink,
} from 'reactstrap';
const ActivateDeactivateUser = (props) => {

 return (
  <Modal
   style={{ width: '25rem' }}
   isOpen={props.actModal}
   toggle={props.toggleactive}
   className={props.className}
  >
   <ModalHeader toggle={props.toggleactive}>Activate/Deactivate</ModalHeader>
   <ModalBody>Are you sure</ModalBody>
   <ModalFooter>
    <Button color="danger" onClick={props.activeuser}>
     ok
                </Button>
    <Button color="secondary" onClick={props.toggleactive}>
     Cancel
                </Button>
   </ModalFooter>
  </Modal>

 )
}
export default ActivateDeactivateUser