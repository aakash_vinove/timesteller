/*eslint-disable */
import React, { useState, useEffect } from 'react';
import {
  Button,
  Row,
  Col,
  FormGroup,
  Form,
  Label,
  Input,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  TabContent, TabPane, Nav, NavItem, NavLink,
} from 'reactstrap';
const AddUser = (props) => {
  return (
    <Modal
      isOpen={props.addModal}
      toggle={props.toggleAdd}
      className={props.className}
      backdrop={props.backdrop}
    >
      <ModalHeader toggle={props.toggleAdd}>User Add</ModalHeader>
      <ModalBody>
        <Form onSubmit={props.addData} enctype="multipart/form-data">
          <Row className="justify-content-center mb-4">
            <Col md="4">
              <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                <Label for="restaurantName" className="mr-sm-2">
                  First Name
          </Label>
                <div>

                  <Input
                    value={props.fields.firstName}
                    type="text"
                    name="firstName"
                    id="firstName"
                    onChange={props.onChange}
                  />
                </div>
                {
                  props.error
                  && props.error.firstName
                  && (<span className="input-error">{props.error.firstName}</span>)
                }
              </FormGroup>
            </Col>
            <Col md="4">
              <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                <Label for="restaurantName" className="mr-sm-2">
                  Last Name
          </Label>
                <div>

                  <Input
                    value={props.fields.lastName}
                    type="text"
                    name="lastName"
                    id="lastName"
                    onChange={props.onChange}
                  />
                </div>
                {
                  props.error
                  && props.error.lastName
                  && (<span className="input-error">{props.error.lastName}</span>)
                }
              </FormGroup>
            </Col>
            <Col md="4">
              <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                <Label for="email" className="mr-sm-2">
                  Email
          </Label>
                <Input
                  value={props.fields.email}
                  type="email"
                  name="email"
                  id="email"

                  onChange={props.onChange}
                />
                {
                  props.error
                  && props.error.email
                  && (<span className="input-error">{props.error.email}</span>)
                }
              </FormGroup>
            </Col>

            <Col className='mt-2' md="4">
              <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                <Label for="email" className="mr-sm-2">
                  Password
          </Label>
                <Input
                  value={props.fields.password}
                  type="password"
                  name="password"
                  id="password"

                  onChange={props.onChange}
                />
                {
                  props.error
                  && props.error.password
                  && (<span className="input-error">{props.error.password}</span>)
                }
              </FormGroup>
            </Col>
            <Col className='mt-2' md="4">
              <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                <Label for="email" className="mr-sm-2">
                  Country Name
          </Label>
                <Input
                  value={props.fields.countryName}
                  type="text"
                  name="countryName"
                  id="countryName"
                  onChange={props.onChange}
                />
                {
                  props.error
                  && props.error.countryName
                  && (<span className="input-error">{props.error.countryName}</span>)
                }
              </FormGroup>
            </Col>
            <Col className='mt-2' md="4">
              <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                <Label for="email" className="mr-sm-2">
                Upload Profile Picture

               </Label>
                <Input
                  type="file"
                  name="profileImage"
                  id="profileImage"
                  onChange={props.imageChange}
                />
                {
                  props && props.imageShow&& props.imageShow.map((data, index) => (
                    <img id="target" id={index} src={data} alt="No Image selected" height="200" width="200" />
                  )
                  )}
              </FormGroup>
            </Col>
          </Row>
        </Form>
      </ModalBody>
      <ModalFooter>
        <Button color="primary" onClick={props.addData} type="submit" >
          Add
  </Button>
        &nbsp;
  <Button color="secondary" onClick={props.toggleAdd}>
          Cancel
  </Button>
      </ModalFooter>
    </Modal>
  )
}
export default AddUser