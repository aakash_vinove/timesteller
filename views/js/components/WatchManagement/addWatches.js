/*eslint-disable */
import React, { useState, useEffect } from 'react';
import {
  Button,
  Row,
  Col,
  FormGroup,
  Form,
  Label,
  Input,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  TabContent, TabPane, Nav, NavItem, NavLink,
} from 'reactstrap';
const AddWatches = (props) => {
  return (
    <Modal
      isOpen={props.addModal}
      toggle={props.toggleAdd}
      className={props.className}
      backdrop={props.backdrop}
    >
      <ModalHeader toggle={props.toggleAdd}>User Watches</ModalHeader>
      <ModalBody>
        <Form onSubmit={props.addData} enctype="multipart/form-data">
          <Row className="justify-content-center mb-4">
            <Col md="4">
              <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                <Label for="restaurantName" className="mr-sm-2">
                  Title
          </Label>
                <div>

                  <Input
                    value={props.title}
                    type="text"
                    name="title"
                    id="title"
                    onChange={props.onChange}
                  />
                </div>
                {
                  props.error
                  && props.error.title
                  && (<span className="input-error">{props.error.title}</span>)
                }
              </FormGroup>
            </Col>
            <Col md="4">
              <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                <Label for="restaurantName" className="mr-sm-2">
                  Facts
          </Label>
                <div>

                  <Input
                    value={props.facts}
                    type="text"
                    name="facts"
                    id="facts"
                    onChange={props.onChange}
                  />
                </div>
                {
                  props.error
                  && props.error.facts
                  && (<span className="input-error">{props.error.facts}</span>)
                }
              </FormGroup>
            </Col>
            <Col md="4">
              <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                <Label for="email" className="mr-sm-2">
                  Highlights
          </Label>
                <Input
                  value={props.highlights}
                  type="text"
                  name="highlights"
                  id="highlights"
                  onChange={props.onChange}
                />
                {
                  props.error
                  && props.error.highlights
                  && (<span className="input-error">{props.error.highlights}</span>)
                }
              </FormGroup>
            </Col>

            <Col className='mt-2' md="4">
              <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                <Label for="email" className="mr-sm-2">
                  Model Number
          </Label>
                <Input
                  value={props.modelNumber}
                  type="text"
                  name="modelNumber"
                  id="modelNumber"

                  onChange={props.onChange}
                />
                {
                  props.error
                  && props.error.modelNumber
                  && (<span className="input-error">{props.error.modelNumber}</span>)
                }
              </FormGroup>
            </Col>
            <Col className='mt-2' md="4">
              <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                <Label for="email" className="mr-sm-2">
                  Price($)
          </Label>
                <Input
                  value={props.price}
                  type="number"
                  name="price"
                  id="price"
                  onChange={props.onChange}
                />
                {
                  props.error
                  && props.error.price
                  && (<span className="input-error">{props.error.price}</span>)
                }
              </FormGroup>
            </Col>
            <Col className='mt-2' md="4">
              <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                <Label for="email" className="mr-sm-2">
                  Short Description
          </Label>
                <Input
                  value={props.shortDescription}
                  type="text"
                  name="shortDescription"
                  id="shortDescription"
                  onChange={props.onChange}
                />
                {
                  props.error
                  && props.error.shortDescription
                  && (<span className="input-error">{props.error.shortDescription}</span>)
                }
              </FormGroup>
            </Col>
            <Col className='mt-2' md="4">
              <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                <Label for="email" className="mr-sm-2">
                  Sub Title
          </Label>
                <Input
                  value={props.subTitle}
                  type="text"
                  name="subTitle"
                  id="subTitle"
                  onChange={props.onChange}
                />
                {
                  props.error
                  && props.error.subTitle
                  && (<span className="input-error">{props.error.subTitle}</span>)
                }
              </FormGroup>
            </Col>
            <Col className='mt-2' md="4">
              <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                <Label for="email" className="mr-sm-2">
                  Summary
          </Label>
                <Input
                  value={props.summary}
                  type="text"
                  name="summary"
                  id="summary"
                  onChange={props.onChange}
                />
                {
                  props.error
                  && props.error.summary
                  && (<span className="input-error">{props.error.summary}</span>)
                }
              </FormGroup>
            </Col>
            <Col className='mt-2' md="4">
              <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                <Label for="email" className="mr-sm-2">
                  Watch Characteristics
          </Label>
                <Input
                  value={props.type}
                  type="text"
                  name="watchCharacteristics"
                  id="type"
                  onChange={props.onChange}
                />
                {
                  props.error
                  && props.error.watchCharacteristics
                  && (<span className="input-error">{props.error.watchCharacteristics}</span>)
                }
              </FormGroup>
            </Col>
            <Col className='mt-2' md="4">
              <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                <Label for="email" className="mr-sm-2">
                  Ideal For
          </Label>
                <Input
                  value={props.idealFor}
                  type="text"
                  name="idealFor"
                  id="idealFor"
                  onChange={props.onChange}
                />
                {
                  props.error
                  && props.error.idealFor
                  && (<span className="input-error">{props.error.idealFor}</span>)
                }
              </FormGroup>
            </Col>
            <Col className='mt-2' md="4">
              <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                <Label for="email" className="mr-sm-2">
                  Style
          </Label>
                <Input
                  value={props.style}
                  type="text"
                  name="style"
                  id="style"
                  onChange={props.onChange}
                />
                {
                  props.error
                  && props.error.style
                  && (<span className="input-error">{props.error.style}</span>)
                }
              </FormGroup>
            </Col>
            <Col className='mt-2' md="4">
              <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                <Label for="email" className="mr-sm-2">
                  Status
          </Label>
                <Input
                  value={props.status}
                  type="text"
                  name="status"
                  id="status"
                  onChange={props.onChange}
                />
                {
                  props.error
                  && props.error.status
                  && (<span className="input-error">{props.error.status}</span>)
                }
              </FormGroup>
            </Col>
            <Col className='mt-2' md="4">
              <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                <Label for="email" className="mr-sm-2">
                  Manufacturing Year
          </Label>
                <Input
                  value={props.manufacturingYear}
                  type="number"
                  name="manufacturingYear"
                  id="manufacturingYear"
                  onChange={props.onChange}
                />
                {
                  props.error
                  && props.error.manufacturingYear
                  && (<span className="input-error">{props.error.manufacturingYear}</span>)
                }
              </FormGroup>
            </Col>
            <Col className='mt-2' md="4">
              <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
              <Label for="email" className="mr-sm-2">
                 
          </Label>
              <select className="form-control" onChange={props.onChangeRating} style={{height:"inherit"}}>
                              <option defaultValue >
                                Ratings
                              </option>
                              <option >1</option>
                              <option>2</option>
                              <option>3</option>
                              <option>4</option>
                              <option>5</option>
                            </select>
                {
                  props.error
                  && props.error.ratings
                  && (<span className="input-error">{props.error.ratings}</span>)
                }
              </FormGroup>
            </Col>
            <Col className='mt-2' md="4">
        <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
          <Label for="email" className="mr-sm-2">
            type
          </Label>
          <Input
            type="text"
            name="type"
            id="type"
            onChange={props.onChange}
          />
         {
                  props.error
                  && props.error.type
                  && (<span className="input-error">{props.error.type}</span>)
                }
        </FormGroup>
      </Col>
            <Col className='mt-2' md="4">
              <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                <Label for="email" className="mr-sm-2">
                  Upload Image
          </Label>
                <Input
                  type="file"
                  name="watchImage"
                  id="watchImage"
                  
                 multiple
                  onChange={props.imageChange}
                />
                {
                  props && props.imageShow&& props.imageShow.map((data, index) => (
                    <img id="target" id={index} src={data} alt="No Image selected" height="200" width="200" />
                  )
                  )}
              </FormGroup>
            </Col>
          </Row>
        </Form>
      </ModalBody>
      <ModalFooter>
        <Button color="primary" onClick={props.addData} type="submit" >
          Add
  </Button>
        &nbsp;
  <Button color="secondary" onClick={props.toggleAdd}>
          Cancel
  </Button>
      </ModalFooter>
    </Modal>
  )
}
export default AddWatches