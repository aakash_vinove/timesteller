// import npm packages
/*eslint-disable */
import React, { Component } from 'react';
import axios from 'axios'
import {
  Container,
  Card,
  CardHeader,
  CardBody,
  Collapse,
  Button,
  Row,
  Col,
  FormGroup,
  Form,
  Label,
  Input,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  TabContent, TabPane, Nav, NavItem, NavLink
} from 'reactstrap';
import Link from 'react-router-dom';
import Pagination from 'react-js-pagination';
import classnames from 'classnames';
import Loader from '../../Common/Loader/Index';
import CustomTable from '../../Common/Table';
import proxy from '../../utils/proxy';
import { mobileRegex } from '../../utils/config';
import AddWatches from './addWatches'
// import EditUser from './editUser'
// import DeleteUser from './deleteUser'

// import local files

import '../../Common/style.css';
import { toasterMessage } from '../../utils/toaster';


class WatchManagement extends Component {
  constructor(props) {
    super(props);
    this.state = {
      watches: null,
      blockedList: '',
      collapse: false,
      filter: '',
      email: '',
      priceCategory: '',
      contactNumber: '',
      modal: false,
      delModal: false,
      addModal: false,
      unblockModal: false,
      activeTab: '1',
      error: {},
      showLoader: false,
      pagination: {
        activePage: 1,
        itemsCountPerPage: 10,
        pageRangeDisplayed: 5,
        totalItemsCount: 0,
      },
      paginationBlocked: {
        activePage: 1,
        itemsCountPerPage: 10,
        pageRangeDisplayed: 5,
        totalItemsCount: 0,
      },
      fields: {
        title: '',
        subTitle: '',
        modelNumber: '',
        price: '',
        summary: '',
        facts: '',
        highlights: '',
        watchCharacteristics: '',
        shortDescription: '',
        type: '',
        idealFor: '',
        style: '',
        status: '',
        manufacturingYear: '',
       
      },
      imageShow: '',
      watchImage: '',
      ratings:0
    };

    this.toggledel = this.toggledel.bind(this);
    this.collapse = this.collapse.bind(this);
    this.handleEdit = this.handleEdit.bind(this);
    this.toggleEdit = this.toggleEdit.bind(this);
    this.toggleAdd = this.toggleAdd.bind(this);
    this.checkValidations = this.checkValidations.bind(this);
    this.handlePageChange = this.handlePageChange.bind(this);
    this.deleteResturant = this.deleteResturant.bind(this);
    this.getBlockList = this.getBlockList.bind(this);
    this.renderActionBlock = this.renderActionBlock.bind(this);
    this.getwatchesData = this.getwatchesData.bind(this);
    this.toggleUnblock = this.toggleUnblock.bind(this);
    this.unblockRestaurnat = this.unblockRestaurnat.bind(this);
    this.exportActive = this.exportActive.bind(this);
    this.exportBlock = this.exportBlock.bind(this);
    this.add = this.add.bind(this);
    this.addData = this.addData.bind(this);
    this.imageChange = this.imageChange.bind(this)
    this.onChange = this.onChange.bind(this)
    this.onChangeRating=this.onChangeRating.bind(this)
  }

  componentDidMount() {
    this.getwatchesData();
  }
  onChangeRating (e) {
    
    this.setState({ ratings: e.target.value });
  };
  imageChange(event) {
    console.log("filsdfsf", event.target.files)
    let watchImage = []
    const totalFiles = event.target.files.length;
    let ImageArray = [];
    for (let i = 0; i < totalFiles; i++) {
      console.log("items", event.target.files[i])
      const file = event.target.files[i];
      if (event.target.files && event.target.files[i]) {
        let reader = new FileReader();
        reader.onload = (e) => {
          watchImage.push(file);
          ImageArray.unshift(e.target.result)
          this.setState({ imageShow: ImageArray });
          this.setState({ watchImage });
        };
        reader.readAsDataURL(file)
      }
    }
  }
  onChange(event) {
    const { fields } = this.state;
    this.setState({ ...this.state, fields: { ...fields, [event.target.name]: event.target.value } });

    // this.setState(prevState => ({
    //   addModal: !prevState.addModal,
    //   fields: { firstName: '', lastName: '', email: '', id: '', countryName: '' }
  }


  addData(e) {

    const { fields, error } = this.state
    console.log("fields:::", fields)
    e.preventDefault();
    this.handleadd()

    // if (!fields.title) {
    //   error.title = 'Title Required';
    // }
    // if (!fields.subTitle) {
    //   error.subTitle = 'Title Required';
    // }
    // if (!fields.modelNumber) {
    //   error.modelNumber = 'modelNumber Required';
    // }
    // if (!fields.price) {
    //   error.price = 'price Required';
    // }
    // if (!fields.summary) {
    //   error.summary = 'summary Required';
    // }
    // if (!fields.facts) {
    //   error.facts = 'facts Required';
    // }
    // if (!fields.highlights) {
    //   error.highlights = 'highlights Required';
    // }
    // if (!fields.watchCharacteristics) {
    //   error.watchCharacteristics = 'watchCharacteristics Required';
    // }
    // if (!fields.shortDescription) {
    //   error.shortDescription = 'shortDescription Required';
    // }
    // if (!fields.type) {
    //   error.type = 'type Required';
    // }
    // if (!fields.idealFor) {
    //   error.idealFor = 'idealFor Required';
    // }
    // if (!fields.style) {
    //   error.style = 'style Required';
    // }
    // if (!fields.status) {
    //   error.status = 'status Required';
    // }
    // if (!fields.manufacturingYear) {
    //   error.manufacturingYear = 'manufacturingYear Required';
    // }
    // this.setState({
    //   error,
    // });
    // if (JSON.stringify(error) !== '{}') {
    //   return null;
    // }
    // if (fields.title&&fields.subTitle&&fields.modelNumber&&fields.price&&fields.summary&&fields.facts ) {
    //   this.handleadd();
    // }
  }

  getwatchesData() {

    if (localStorage.getItem('token')) {
      let token = localStorage.getItem("token");
      axios
        .get(`http://timestellerbackend.n1.iworklab.com:3212/watchAdminList`,
          {
            headers: {
              Authorization: "Bearer " + token,
            }
          })
        .then(res => {
          console.log("result", res.data.response.watches)
          let watches = [];
          watches = res.data.response.watches.map((watches) => {
            const tempObj = {
              ...watches,
              action: this.renderAction(watches),

            };
            console.log(tempObj, 'temo');
            return tempObj;
          })

          this.setState({
            watches,
          });
        })
        .catch(err => {

          console.log(err)
        });
      //   proxy
      //     .call('get', 'getUsersAPI', null, null, Bearer+localStorage.getItem('token'))
      //     .then((response) => {
      //       console.log(response.data.data, 'response.data.data');
      //       let watches = [];
      //       if (
      //         response &&
      //         (response.status === 200 || response.status === 201) &&
      //         response.data &&
      //         response.data.data
      //       ) {

      //         this.setState({
      //           showLoader: false,
      //           pagination: {
      //             ...this.state.pagination,
      //             totalItemsCount: response.data.pageCount,
      //           },

      //         }, () => console.log('frm setstate', this.state.pagination));

      //         watches = response.data.data.map((watches) => {


      //           const tempObj = {
      //             ...watches,
      //             action: this.renderAction(watches),
      //           };


      //           // if (response.data.result && response.data.result.length) {
      //           //   const index = response.data.result.findIndex(val => val.ownerID === watches._id);
      //           //   if (index !== -1) {
      //           //     tempObj = {
      //           //       ...watches,
      //           //       ...response.data.result[index],
      //           //       action: this.renderAction(watches),
      //           //     };

      //           //   }
      //           // }
      //           console.log(tempObj, 'temo');
      //           return tempObj;
      //         }, console.log(watches, 'vvvv'));
      //       }
      //       this.setState({
      //         watches,
      //       });
      //     })
      //     .catch((err) => {
      //       toasterMessage('error', err.response.data.message);
      //     });
    }
    else {
      this.state.showLoader = true;
      this.props.history.push('/');
    }

  }
  getBlockList() {
    this.toggleTab('2');
    const currentPage = this.state.paginationBlocked.activePage;
    const currentPag = { currentPage };
    this.setState({
      showLoader: true,
      filter: '',
    });
    if (localStorage.getItem('token')) {
      proxy
        .call('get', 'deactivateResturantList', currentPag, null, localStorage.getItem('token'))
        .then((response) => {
          console.log('reee', response);
          let watches = [];
          if (
            response &&
            (response.status === 200 || response.status === 201) &&
            response.data &&
            response.data.data
          ) {

            this.setState({
              showLoader: false,
              paginationBlocked: {
                ...this.state.paginationBlocked,
                totalItemsCount: response.data.pageCount,
              },
            });
            watches = response.data.data.map((watches) => {

              const tempObj = {
                ...watches,
                action: this.renderActionBlock(watches),
              };

              // if (response.data.result && response.data.result.length) {
              //   const index = response.data.result.findIndex(val => val.ownerID === watches._id);
              //   if (index !== -1) {
              //     tempObj = {
              //       ...watches,
              //       ...response.data.result[index],
              //       action: this.renderAction(watches),
              //     };

              //   }
              // }

              return tempObj;
            });
          }
          this.setState({
            blockedList: watches,
          });
        })
        .catch((err) => {
          toasterMessage('error', err.response.data.message);
        });
    } else {
      this.state.showLoader = true;
      this.props.history.push('/');
    }
  }

  applyFilters(e) {
    e.preventDefault();
    if (this.state.filter === '') {
      alert('enter some text to filter');
    } else {
      this.setState({ showLoader: true });

      if (localStorage.getItem('token')) {
        proxy
          .call('get', `filterResturantActive/${this.state.filter}`, null, null, localStorage.getItem('token'))
          .then((response) => {
            // console.log(response.data.data, 'response
            let watches = [];
            if (
              response &&
              (response.status === 200 || response.status === 201)

            ) {

              this.setState({
                showLoader: false,
                pagination: {
                  ...this.state.pagination,
                  totalItemsCount: response.data.pageCount,
                },
              });
              watches = response.data.data.map((watches) => {
                console.log(watches, 'rest');
                const tempObj = {
                  ...watches,
                  action: this.renderAction(watches),
                };
                console.log(tempObj, 'temp');
                return tempObj;
              });
            } else {
              watches = [];
            }

            this.setState({
              watches,
            }, () => console.log(watches));
          })
          .catch((err) => {
            toasterMessage('error', err.response.data.message);
          });
      } else {
        this.state.showLoader = true;
        this.props.history.push('/');
      }


    }
  }
  applyFiltersBlock(e) {
    e.preventDefault();
    alert('fbjbj');
    if (localStorage.getItem('token')) {
      proxy
        .call('get', `filterResturantDeactivate/${this.state.filter}`, null, null, localStorage.getItem('token'))
        .then((response) => {
          console.log(response.data.data, 'response');
          let watches = [];
          if (
            response &&
            (response.status === 200 || response.status === 201)

          ) {
            this.setState({
              showLoader: false,
              paginationBlocked: {
                ...this.state.paginationBlocked,
                totalItemsCount: response.data.pageCount,
              },
            });
            watches = response.data.data.map((watches) => {
              console.log(watches, 'rest');
              const tempObj = {
                ...watches,
                action: this.renderAction(watches),
              };
              console.log(tempObj, 'temp');
              return tempObj;
            });
          }
          this.setState({
            blockedList: watches,
          });
        })
        .catch((err) => {
          toasterMessage('error', err.response.data.message);
        });
    } else {
      this.state.showLoader = true;
      this.props.history.push('/');
    }
  }

  clearFilters(e) {

    e.preventDefault();
    if (this.state.activeTab === '1') { this.getwatchesData(); } else {
      this.getBlockList();
    }
    this.setState({ filter: '' });
  }

  checkValidations(e) {

    e.preventDefault();
    const { fields, error } = this.state;
    if (!fields.firstName) {
      error.name = 'First name Required';
    }
    this.setState({
      error,
    });
    if (JSON.stringify(error) !== '{}') {
      return null;
    }
    if (fields.firstName) {
      this.handleEdit();
    }
  }

  edit(user) {

    console.log("adasd::", user)
    this.setState({
      ...this.state,
      fields: {
        firstName: user.firstName,
        lastName: user.lastName,
        email: user.email,
        id: user._id,
        countryName: user.countryName


      },

    }, () => console.log(this.state.fields, 'idddd'));
    console.log(':: edit id ', user);
    this.toggleEdit();
  }
  toggleEdit() {
    console.log(',mkkkkk', this.state.modal);
    this.setState(prevState => ({
      modal: !prevState.modal,
    }));
  }
  toggleAdd() {

    this.setState(prevState => ({
      addModal: !prevState.addModal,
      fields: { firstName: '', lastName: '', email: '', id: '', countryName: '' }

    }));
  }
  handleEdit() {
    let token = localStorage.getItem('token')
    console.log(this.state.fields, 'edited');
    const body = {
      firstName: this.state.fields.firstName,
      lastName: this.state.fields.lastName,
      watchImage: this.state.fields.watchImage,
      countryName: this.state.fields.countryName
    };
    let form = new FormData();
    for (var key in body) {
      console.log("key>>>", key);
      form.append(key, body[key]);
    }
    const id = this.state.fields.id;
    const Id = { id };
    console.log(Id, 'id');

    if (localStorage.getItem('token')) {
      this.setState({ showLoader: true });

      axios
        .put(`http://timestellarbackend.n1.iworklab.com:3212/editProfileAPI/${id}`, form,
          {
            headers: {
              Authorization: "Bearer " + token,
            }
          })
        .then((response) => {
          if (response &&
            (response.status === 200 || response.status === 201) &&
            response.data) {

            toasterMessage(
              'success',
              (
                response
                && response.data

                && response.data.message
              ) || (
                response
                && response.message
              ),
            );

            this.getwatchesData();
            this.toggleEdit();
            this.setState({
              ...this.state,

              showLoader: false,
            });

          }
        })
        .catch((err) => {
          alert(err);
          // this.setState({ showLoader: false });
          // toasterMessage(
          //   'error',
          //   (
          //     err
          //     && err.response
          //     && err.response.data
          //     && err.response.data.message
          //   ) || (
          //     err
          //     && err.message
          //   ),
          // );
        });

    } else { this.props.history.push('/'); }

  }
  handleadd() {
    let token = localStorage.getItem('token')

    const body = {
      title: this.state.fields.title,
      subTitle: this.state.fields.subTitle,
      modelNumber: this.state.fields.modelNumber,
      price: this.state.fields.price,
      summary: this.state.fields.summary,
      facts: this.state.fields.facts,
      highlights: this.state.fields.highlights,
      watchCharacteristics: this.state.fields.watchCharacteristics,
      shortDescription: this.state.fields.shortDescription,
      type:"abc",
      idealFor: this.state.fields.idealFor,
      style: this.state.fields.style,
      status: this.state.fields.status,
      manufacturingYear: this.state.fields.manufacturingYear,
      ratings:parseInt(this.state.ratings),
      brandName:"sds"

    };
    console.log("body data::", body)
    let form = new FormData();
    for (var key in body) {
      console.log("key>>>", key);
      form.append(key, body[key]);
    }
    for (let i = 0; i < this.state.watchImage.length; i++) {

      console.log("asdasdsadsad:::", this.state.watchImage[i])
      form.append('watchImage', this.state.watchImage[i]);
    }

    const id = this.state.fields.id;
    const Id = { id };
    console.log(Id, 'id');

    if (localStorage.getItem('token')) {
      this.setState({ showLoader: true });
      axios
        .post(`http://timestellarbackend.n1.iworklab.com:3212/addWatchAPI`, form, {
          headers: {
            Authorization: "Bearer " + token,
          }
        })
        .then((response) => {
          if (response &&
            (response.status === 200 || response.status === 201) &&
            response.data) {

            toasterMessage(
              'success',
              (
                response
                && response.data

                && response.data.message
              ) || (
                response
                && response.message
              ),
            );

            this.getwatchesData();
            this.toggleAdd();
            this.setState({
              ...this.state,

              showLoader: false,
            });

          }
        })
        .catch((err) => {
          
          this.setState({ showLoader: false });
          toasterMessage(
            'error',
            (
              err
              && err.response
              && err.response.data
              && err.response.data.message
            ) || (
              err
              && err.message
            ),
          );
        });

    } else { this.props.history.push('/'); }

  }
  handlePageChange(pageNumber) {
    if (this.state.activeTab === '1') {
      console.log(pageNumber);
      this.setState({
        ...this.state, pagination: { activePage: pageNumber },
      }, () => { this.getwatchesData(); });
    } else {
      console.log(pageNumber);
      this.setState({
        ...this.state, paginationBlocked: { activePage: pageNumber },
      }, () => { this.getBlockList(); });
    }

  }
  view(id) {
    console.log(id._id);
    const Id = id._id;
    this.props.history.push(`/admin/watches/${Id}`);
    // this.setState({
    //   watchesName: resturant.name,
    //   email: resturant.email,
    //   contactNumber: resturant.contactNumber,
    //   priceCategory: resturant.priceCategory,
    // });
    // this.toggle();
  }
  remove(resturant) {
    console.log("USers", resturant._id)
    this.setState({ fields: { id: resturant._id, email: resturant.email } });
    this.toggledel();

  }
  toggledel() {
    this.setState(prevState => ({
      delModal: !prevState.delModal,
    }));
  }
  toggleUnblock() {
    this.setState(prevState => ({
      unblockModal: !prevState.unblockModal,
    }));
  }
  toggleTab(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab,
      });
    }
  }
  Unblock(item) {
    this.setState({ fields: { id: item._id, email: item.email } });
    this.toggleUnblock();
  }
  deleteResturant() {

    this.toggledel();
    const id = this.state.fields.id;
    const Id = { id };
    const body = {
      email: this.state.fields.email,
    };
    if (localStorage.getItem('token')) {
      let token = localStorage.getItem('token')
      this.setState({ showLoader: true });
      axios
        .delete(`http://timestellarbackend.n1.iworklab.com:3212/deleteUserAPI/${id}`,
          {
            headers: {
              Authorization: "Bearer " + token,
            }
          })
        .then((response) => {

          if (
            response
            && (
              response.status === 200
              || response.status === 201
            )
            && response.data


          ) {
            toasterMessage(
              'success',
              (
                response
                && response.data

                && response.data.message
              ) || (
                response
                && response.message
              ),
            );
            this.getwatchesData();

            this.setState({
              ...this.state,

              showLoader: false,

            });
          }
        })
        .catch((err) => {
          console.log("err", err)
          toasterMessage(
            'error',
            "Unable to delete this user "
          );
          this.setState({
            ...this.state,

            showLoader: false,

          })
        });

    } else {

      this.props.history.push('/');
    }
  }
  unblockRestaurnat() {
    const body = {
      email: this.state.fields.email,
    };
    this.toggleUnblock();
    const id = this.state.fields.id;
    const Id = { id };
    if (localStorage.getItem('token')) {
      this.setState({ showLoader: true });
      proxy.call('put', 'resturantActivate', Id, body, localStorage.getItem('token'))
        .then((response) => {

          if (
            response
            && (
              response.status === 200
              || response.status === 201
            )
            && response.data


          ) {
            toasterMessage(
              'success',
              (
                response
                && response.data

                && response.data.message
              ) || (
                response
                && response.message
              ),
            );

            this.getBlockList();

            this.setState({
              ...this.state,

              showLoader: false,

            });
          }
        })
        .catch((err) => {
          console.log(err)
          toasterMessage(
            'error',
            (
              err
              && err.response
              && err.response.data

            ) || (
              err
              && err.message
            ),
          );
        });

    } else {

      this.props.history.push('/');
    }
  }
  collapse() {
    this.setState(prevState => ({
      collapse: !prevState.collapse,
    }));
  }
  tableData() {
    return {
      labels: [
        {
          label: 'Model Number',
          value: `modelNumber`,
          isDate: false,
        },
        {
          label: 'Title',
          value: `title`,
          isDate: false,
        },

        {
          label: 'Price',
          value: 'price',
          isDate: false,
        },
        {
          label: 'Sub Title',
          value: 'subTitle',
          isDate: false,
        },
        {
          label: 'Type',
          value: 'type',
          isDate: false,
        },
        {
          label: 'Manufacturing Year',
          value: 'manufacturingYear',
          isDate: false,
        },


        // {
        //   label: 'Action',
        //   value: 'action',
        //   isDate: false,
        // },
      ],
      content: this.state.watches,
    };
  }
  blockTable() {
    return {
      labels: [
        {
          label: 'watches Name',
          value: 'name',
          isDate: false,
        },
        {
          label: 'Email',
          value: 'email',
          isDate: false,
        },
        {
          label: 'Contact Number',
          value: 'contactNumber',
          isDate: false,
        },
        {
          label: 'Action',
          value: 'action',
          isDate: false,
        },
      ],
      content: this.state.blockedList,
    };
  }

  modal() {
    return <div />;
  }
  
  exportActive() {
    if (localStorage.getItem('token')) {
      proxy
        .call('get', 'exportWatchList', null, null, localStorage.getItem('token'))
        .then((res) => {
          console.log(res)
          
          const url = 'http://timestellarbackend.n1.iworklab.com:3212/exportWatchList';
          window.open(url, '_blank')
            .focus();
        });
    }
  }
  exportBlock() {
    if (localStorage.getItem('token')) {
      proxy
        .call('get', 'exportToExcelDeactiveResturant', null, null, localStorage.getItem('token'))
        .then((res) => {
          const url = 'http://realtimedeals.n1.iworklab.com:3005/v1/resturant/exportToExcelDeactiveResturant';
          window.open(url, '_blank')
            .focus();
        });
    }
  }

  renderAction(watches) {
    return (
      <div style={{ textAlign: 'center' }}>
        {/* <span
          className="action-icon edit-icon"
          onClick={() => {
            this.view(watches);
          }}
        >
          <i className="icon-eye" />
        </span> */}
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <span
          className="action-icon edit-icon"
          onClick={() => { this.edit(watches); }}
        >
          <i className="icon-pencil" />
        </span>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <span
          className="action-icon remove-icon"
          onClick={() => {
            this.remove(watches);
          }}
        >
          Delete
        </span>
      </div>
    );
  }

  add() {
    this.toggleAdd()
  }

  renderActionBlock(watches) {
    return (
      <div style={{ textAlign: 'center' }}>
        <span
          className="action-icon edit-icon"
          onClick={() => {
            this.view(watches);
          }}
        >
          <i className="icon-eye" />
        </span>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <span
          className="action-icon edit-icon"
          onClick={() => { this.edit(watches); }}
        >
          <i className="icon-pencil" />
        </span>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <span
          className="action-icon remove-icon"
          onClick={() => {
            this.Unblock(watches);
          }}
        >
          Unblock
        </span>
      </div>
    );
  }
  renderFilter() {
    return (
      <Form>
        <Row className="justify-content-center mb-4">
          <Col md="6">
            <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
              <Label for="watchesName" className="mr-sm-2">
                Search
              </Label>
              <Input
                value={this.state.filter}
                type="text"
                placeholder="Search By Name, E-Mail, Contact No. "
                name="resturantName"
                id="watchesName"
                onChange={(event) => {
                  this.setState({ filter: event.target.value });
                }}
              />
            </FormGroup>
          </Col>


        </Row>
        <Row className="justify-content-center ">
          <Col md="6">
            <Button onClick={(e) => { this.applyFilters(e); }} color="primary" type="submit">
              Apply Filter
            </Button>
            <Button onClick={(e) => { this.clearFilters(e); }} color="secondary button-clear">Clear</Button>
          </Col>
        </Row>

      </Form>
    );
  }
  renderFilterBlocked() {
    return (
      <Form>
        <Row className="justify-content-center mb-4">
          <Col md="6">
            <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
              <Label for="watchesName" className="mr-sm-2">
                Search
              </Label>
              <Input
                value={this.state.filter}
                type="text"
                placeholder="Search By Name, E-Mail, Contact No. "
                name="resturantName"
                id="watchesName"
                onChange={(event) => {
                  this.setState({ filter: event.target.value });
                }}
              />
            </FormGroup>
          </Col>


        </Row>
        <Row className="justify-content-center ">
          <Col md="6">
            <Button onClick={(e) => { this.applyFiltersBlock(e); }} color="primary" type="submit">
              Apply Filter
            </Button>
            <Button onClick={(e) => { this.clearFilters(e); }} color="secondary button-clear">Clear</Button>
          </Col>
        </Row>

      </Form>
    );
  }

  render() {
    console.log("watches Are", this.state.watches)
    return (
      <div>
        {this.state.showLoader ? <Loader /> : ''}
        <div>
          {/* <div className={this.state.showLoader ? 'blurr' : ' '}> */}

          <Nav tabs>
            <NavItem>
              <NavLink
                className={classnames({ active: this.state.activeTab === '1' })}
                onClick={() => { this.getwatchesData(); }}
              >
                Active List
              </NavLink>
            </NavItem>

            {/* <NavItem>
              <NavLink
                className={classnames({ active: this.state.activeTab === '2' })}
                onClick={() => { this.getBlockList(); }}
              >
                Block List
              </NavLink>
            </NavItem> */}
          </Nav>

          <TabContent activeTab={this.state.activeTab}>
            <TabPane tabId="1">

              <Row>
                <Col sm="12">
                  <Card>
                    <CardHeader className="bg-dark text-white" >Watches </CardHeader>
                    <CardBody>
                      <div>
                        <Row>
                          <Col md="12">
                            <Button onClick={this.add}>Add</Button>
                            <Button onClick={this.exportActive} style={{ marginBottom: '1rem', float: 'right' }} >Export</Button>
                            {/* <Button
                              color="primary"
                              onClick={this.collapse}
                              style={{ marginBottom: '1rem', float: 'right' }}
                            >Filter
                            </Button> */}
                          </Col>
                        </Row>

                        <Collapse
                          className="collapse-filter"
                          isOpen={this.state.collapse}
                        >
                          <Card className="filter-container">{this.renderFilter()}</Card>
                        </Collapse>
                      </div>

                      <CustomTable data={this.tableData()} />

                    </CardBody>
                  </Card>
                </Col>

              </Row>
              {/* <Pagination
                activePage={this.state.pagination.activePage}

                totalItemsCount={this.state.pagination.totalItemsCount}
                pageRangeDisplayed={5}
                onChange={this.handlePageChange}
              /> */}

            </TabPane>
            <TabPane tabId="2">

              <Row>
                <Col sm="12">
                  <Card>
                    <CardHeader className="bg-dark text-white" >Blocked watches</CardHeader>
                    <CardBody>
                      <div>
                        <Row>
                          <Col md="12">
                            <Button onClick={this.exportBlock} >Export</Button>
                            <Button
                              color="primary"
                              onClick={this.collapse}
                              style={{ marginBottom: '1rem', float: 'right' }}
                            >Filter
                            </Button>
                          </Col>
                        </Row>
                        <Collapse
                          className="collapse-filter"
                          isOpen={this.state.collapse}
                        >
                          <Card className="filter-container">{this.renderFilterBlocked()}</Card>
                        </Collapse>
                      </div>

                      <CustomTable data={this.blockTable()} />

                    </CardBody>
                  </Card>
                </Col>

              </Row>
              <Pagination
                activePage={this.state.paginationBlocked.activePage}

                totalItemsCount={this.state.paginationBlocked.totalItemsCount}
                pageRangeDisplayed={5}
                onChange={this.handlePageChange}
              />
            </TabPane>
          </TabContent>
          <Container >
            <AddWatches
              addData={this.addData}
              onChange={this.onChange}
              addModal={this.state.addModal}
              toggleAdd={this.toggleAdd}
              backdrop={this.state.backdrop}
              fields={this.state.fields}
              error={this.state.error}
              className={this.props.className}
              imageShow={this.state.imageShow}
              imageChange={this.imageChange}
              onChangeRating={this.onChangeRating}
            />
            {/* <EditUser 
            modal={this.state.modal}
            toggleEdit={this.toggleEdit}
            className={this.props.className}
            backdrop={this.state.backdrop}
            checkValidations={this.checkValidations}
            fields={this.state.fields}
            onChange={this.onChange}
            
                />
          <DeleteUser 
          delModal={this.state.delModal}
          toggledel={this.toggledel}
          className={this.props.className}
          deleteResturant={this.deleteResturant}
          /> */}
            {/* delete modal starts */}

            {/* delet modal ends */}
            {/* unblock modal starts */}
            <Modal
              style={{ width: '25rem' }}
              isOpen={this.state.unblockModal}
              toggle={this.toggleUnblock}
              className={this.props.className}
            >
              <ModalHeader toggle={this.toggleUnblock}>Delete</ModalHeader>
              <ModalBody>Are you sure want to Unblock ?</ModalBody>
              <ModalFooter>
                <Button color="danger" onClick={this.unblockRestaurnat}>
                  Unblock
                </Button>
                <Button color="secondary" onClick={this.toggleUnblock}>
                  Cancel
                </Button>
              </ModalFooter>
            </Modal>
            {/* unblock modal ends */}


            {/* <Pagination pagination={this.state.pagination} /> */}
          </Container>
        </div>

      </div>
    );
  }
}

export default WatchManagement;
