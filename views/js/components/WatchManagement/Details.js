import React, { Component } from 'react';
import {
  Container,
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  Collapse,
  Button,
  Row,
  Col,
  FormGroup,
  Form,
  Label,
  Input,
} from 'reactstrap';
import { toasterMessage } from '../../utils/toaster';
import proxy from '../../utils/proxy';
import Loader from '../../Common/Loader/Index';
import CustomTable from '../../Common/Table';
import { BASE_URL } from '../../utils/config';

export default class Details extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showLoader: false,
      userDeal: [],
      userInfo: '',
      userPoints: '',
      baseUrl: 'http://realtimedeals.n1.iworklab.com:3005/',

    };
    this.getDetails = this.getDetails.bind(this);
    this.getPersonalInfo = this.getPersonalInfo.bind(this);
  }
  componentDidMount() {
    this.getDetails();
    this.getPersonalInfo();
    this.getUserPoints();
  }
  getDetails() {
    const userId = this.props.match.params.id;
    const resturanid = { userId };
    if (localStorage.getItem('token')) {
      console.log('token', localStorage.getItem('token'));
      this.setState({ showLoader: true });
      proxy.call('get', 'getUserDeal', resturanid, null, localStorage.getItem('token'))
        .then((response) => {
          console.log(response, ';');
          if (
            response
            && (
              response.status === 200
              || response.status === 201
            )
            && response.data
            && response.data

          ) {

            this.setState({
              ...this.state,
              userDeal: response.data.popularDeals,
              showLoader: false,


            });
          }
        })
        .catch((err) => {
          // this.setState({ showLoader: false });
          toasterMessage(
            'error',
            (
              err
              && err.response
              && err.response.data
              && err.response.data.message
            ) || (
              err
              && err.message
            ),
          );
        });
    } else {
      // this.setState({ showLoader: false });
      this.props.history.push('/');
    }

  }
  getPersonalInfo() {
    const userId = this.props.match.params.id;
    const resturanid = { userId };
    if (localStorage.getItem('token')) {

      this.setState({ showLoader: true });
      proxy.call('get', `profileUser/${userId}`, null, null, localStorage.getItem('token'))
        .then((response) => {
          console.log(response, 'oersonalInfop');
          if (
            response
            && (
              response.status === 200
              || response.status === 201
            )
            && response.data
            && response.data.data

          ) {

            this.setState({
              ...this.state,
              userInfo: response.data.data,
              showLoader: false,


            }, () => console.log(this.state));
          }
        })
        .catch((err) => {
          // this.setState({ showLoader: false });
          toasterMessage(
            'error',
            (
              err
              && err.response
              && err.response.data
              && err.response.data.message
            ) || (
              err
              && err.message
            ),
          );
        });
    } else {
      // this.setState({ showLoader: false });
      this.props.history.push('/');
    }
  }
  getUserPoints() {
    const userId = this.props.match.params.id;
    const resturanid = { userId };
    if (localStorage.getItem('token')) {

      this.setState({ showLoader: true });
      proxy.call('get', `usersPoint/${userId}`, null, null, localStorage.getItem('token'))
        .then((response) => {
          console.log(response, 'points');
          if (
            response
            && (
              response.status === 200
              || response.status === 201
            )
            && response.data
            && response.data.data

          ) {

            this.setState({
              ...this.state,
              userPoints: response.data.data,
              showLoader: false,


            }, () => console.log(this.state, 'getUserPoints'));
          }
        })
        .catch((err) => {
          // this.setState({ showLoader: false });
          toasterMessage(
            'error',
            (
              err
              && err.response
              && err.response.data
              && err.response.data.message
            ) || (
              err
              && err.message
            ),
          );
        });
    } else {
      // this.setState({ showLoader: false });
      this.props.history.push('/');
    }
  }
  History() {
    return {
      labels: [
        //   {
        //   label: 'Title',
        //   value: 'dealTittle',
        //   isDate: false,
        // },
        {
          label: 'Title',
          value: 'dealTitle',
          isDate: false,
        },
        {
          label: 'Restaurant Name',
          value: 'restaurantName',
          isDate: false,
        }, {
          label: 'Transaction Date',
          value: 'claimedTime',
          isDate: true,
        },
        {
          label: 'People Number',
          value: 'PeopleNumber',
          isDate: false,
        },
        {
          label: 'Status',
          value: 'status',
          isDate: false,
        }],
      content: (
        this.state.userDeal
          && this.state.userDeal.length
          && this.state.userDeal.map(customer => ({

            ...customer,
            status: (
              <div style={{ textAlign: 'center' }}>
                <span >
                  {customer.status}
                  {/* <i className="icon-eye" /> */}
                </span>
              </div>
            ),
            peopleNumber: (
              <div style={{ textAlign: 'center' }}>
                <span >
                  {customer.PeopleNumber}
                  <i className="icon-eye" />
                </span>
              </div>
            ),

          }))
      ) || null,


    };
  }
  Point() {
    return {
      labels: [
        //   {
        //   label: 'Title',
        //   value: 'dealTittle',
        //   isDate: false,
        // },
        {
          label: 'Deal Title',
          value: 'dealTitle',
          isDate: false,
        },
        {
          label: 'Points Earned',
          value: 'PeopleNumber',
          isDate: false,
        }, {
          label: 'Transaction Date',
          value: 'claimedTime',
          isDate: true,
        },
        // {
        //   label: 'Expiry Time',
        //   value: 'dealExpiryTime',
        //   isDate: false,
        // },
        //  {
        //   label: 'Deal Type',
        //   value: 'dealType',
        //   isDate: false,
        // },
        {
          label: 'Status',
          value: 'status',
          isDate: false,
        }],
      content: (this.state.userPoints && this.state.userPoints.length &&
        this.state.userPoints.map(item => ({
          ...item,

          dealTitle: (
            item.dealId.dealTitle
          ),
        }))),
    };
  }
  render() {
    return (
      <div>
        {this.state.showLoader ? <Loader /> : ''}
        <Card className="bg-light" >
          <CardHeader className="bg-dark text-light">User Details</CardHeader>

          <CardBody>
            <div className="row">
              <div className="col-lg-2 col-md-6">
           Name
                <br />
                {this.state.userInfo.name}
              </div>
              <div className="col-lg-2 col-md-6">
                      Email
                <br />
                {this.state.userInfo.email}
              </div>
              <div className="col-lg-2 col-md-6">
           Total Points
                <br />
                {this.state.userInfo.rewards}
              </div>
              <div className="col-lg-2 col-md-6">
           Category
                <br />
                {this.state.userInfo.categoryName}
              </div>
              <div className="col-lg-2 col-md-6">
           Category  Icon
                <br />
                <img style={{ height: '50px', width: '50px' }} src={this.state.baseUrl + this.state.userInfo.categoryImage} alt="icons" />
                {/* {this.state.userInfo.categoryName} */}
              </div>
            </div>

          </CardBody>
        </Card>
        <Card>
          <CardBody>
            <CardHeader className="bg-dark text-white">Claimed / Reedemed History</CardHeader>
            <CustomTable data={this.History()} />
          </CardBody>


        </Card>
        <Card>
          <CardBody>
            <CardHeader className="bg-dark text-white">Point details</CardHeader>
            <CustomTable data={this.Point()} />
          </CardBody>
        </Card>
      </div>
    );
  }
}
