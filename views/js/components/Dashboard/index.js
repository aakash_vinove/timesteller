// import npm packages
import React, { Component } from 'react';
import {
  Row, Col, Card, CardHeader, CardBody, FormGroup,
  Form,
  Label,
  Input,
  Button,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Collapse,
  Badge,
  ButtonDropdown,
  ButtonGroup,
  ButtonToolbar,
  CardTitle,
  Dropdown,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Progress,
  Table,
} from 'reactstrap';
import { Bar, Line } from 'react-chartjs-2';
import { toasterMessage } from '../../utils/toaster';
import proxy from '../../utils/proxy';
import Loader from '../../Common/Loader/Index';

class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeUsers: '',
      inActive: '',
      activeDeal: '',
      inActiveDeal: '',
      totalUser: '',
      totalDeal: '',
      totalResturant: '',
      inActiveResturants: '',
      activeResturant: '',
      showLoader: false,
    };
    this.getDashboardData = this.getDashboardData.bind(this);
  }
  componentDidMount() {
    this.getDashboardData();
  }
  getDashboardData() {
    if (localStorage.getItem('token')) {

      // this.setState({ showLoader: true });

      // proxy.call('get', 'countData', null, null, localStorage.getItem('token'))
      //   .then((response) => {
      //     console.log(response.data, 'response');
      //     if (response &&
      //       (response.status === 200 || response.status === 201) &&
      //       response.data &&
      //       response.data
      //     ) {
      //       this.setState({
      //         ...this.state,
      //         activeUsers: response.data.activeUser,
      //         inActiveUsers: response.data.inActiveUser,
      //         activeDeal: response.data.activeDeal,
      //         inActiveDeal: response.data.InActiveDeal,
      //         totalUser: response.data.totalUser,
      //         totalDeal: response.data.totalDeal,
      //         activeResturant: response.data.activeResturant,
      //         inActiveResturants: response.data.inActiveResturants,
      //         totalResturant: response.data.totalResturant,
      //         showLoader: false,


      //       });

      //     }
      //   })
      //   .catch((err) => {
      //     // this.setState({ showLoader: false });
      //     toasterMessage(
      //       'error',
      //       (
      //         err
      //         && err.response
      //         && err.response.data
      //         && err.response.data.message
      //       ) || (
      //         err
      //         && err.message
      //       ),
      //     );
      //   });

    } else { this.props.history.push('/'); }
  }
  // tableData() {
  //   return {
  //     labels: [{
  //       label: 'Title',
  //       value: 'foodType',
  //       isDate: false,
  //     }, {
  //       label: 'Created At',
  //       value: 'createdAt',
  //       isDate: true,
  //     }, {
  //       label: 'Updated At',
  //       value: 'updatedAt',
  //       isDate: true,
  //     }, {
  //       label: 'Action',
  //       value: 'action',
  //       isDate: false,
  //     }],
  //     // content: (
  //     //   this.state.data
  //     //   && this.state
  //     //   && this.state.data.map(deal => ({
  //     //     ...deal,
  //     //     action: (
  //     //       <div style={{ textAlign: 'center' }}>
  //     //         <span
  //     //           className="action-icon edit-icon"
  //     //           onClick={() => { this.edit(deal); }}
  //     //         >
  //     //           <i className="icon-pencil" />
  //     //         </span>
  //     //         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  //     //         <span
  //     //           className="action-icon remove-icon"
  //     //           onClick={() => { this.remove(deal); }}
  //     //         >
  //     //           <i className="icon-trash" />
  //     //         </span>
  //     //       </div>
  //     //     ),
  //     //   }))
  //     // ) || null,
  //   };

  render() {

    return (
      <div className="animated fadeIn" >
        {this.state.showLoader ? <Loader /> : ''}
        <Row>
          <Col xs="12" sm="6" lg="2">
            <Card className="text-white bg-info shadow">
              <CardBody className="pb-0">

                {/* <Line data={cardChartData2} options={cardChartOpts2} height={70} /> */}

                <div className="text-value">55</div>
                <div className="pb-3">Total Users </div>
              </CardBody>
            </Card>

          </Col>
          <Col xs="12" sm="6" lg="2">
            <Card className="text-white bg-dark">
              <CardBody className="pb-0">

                {/* <Line data={cardChartData2} options={cardChartOpts2} height={70} /> */}

                <div className="text-value">40</div>
                <div className="pb-3">Active Users</div>
              </CardBody>
            </Card>

          </Col>
          <Col xs="12" sm="6" lg="2">
            <Card className="text-white bg-danger shadow">
              <CardBody className="pb-0">

                {/* <Line data={cardChartData2} options={cardChartOpts2} height={70} /> */}

                <div className="text-value">40</div>
                <div className="pb-3">Inactive Customers</div>
              </CardBody>
            </Card>

          </Col>
          <Col xs="12" sm="6" lg="2">
            <Card className="text-white bg-primary shadow">
              <CardBody className="pb-0">

                {/* <Line data={cardChartData2} options={cardChartOpts2} height={70} /> */}

                <div className="text-value">50</div>
                <div className="pb-3">Total Watch Added</div>
              </CardBody>
            </Card>

          </Col>
        </Row>
        <Row>
          <Col sm="12" lg="4" md="4">
            <Card className="shadow">
              <CardHeader className="bg-dark text-white" >
                User Counts
              </CardHeader>
              <CardBody >
                <Table >
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Title</th>
                      <th>Count</th>

                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <th scope="row">1</th>
                      <td>Active User:-</td>
                      <td>55</td>


                    </tr>
                    <tr>
                      <th scope="row">2</th>
                      <td>Inactive User:-</td>
                      <td>40</td>


                    </tr>
                    <tr>
                      <th scope="row">3</th>
                      <td>Total Users:-</td>
                      <td>50</td>


                    </tr>

                  </tbody>
                </Table>

              </CardBody>

            </Card>
          </Col>
          {/* <Col sm="12" lg="4" md="4">
            <Card className="shadow">
              <CardHeader className="bg-dark text-white" >
                Deals Counts
              </CardHeader>
              <CardBody >
                <Table >
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Title</th>
                      <th>Count</th>

                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <th scope="row">1</th>
                      <td>Active Deals:-</td>
                      <td>{this.state.activeDeal}</td>

                    </tr>
                    <tr>
                      <th scope="row">2</th>
                      <td>Inactive Deals:-</td>
                      <td>{this.state.inActiveDeal}</td>


                    </tr>
                    <tr>
                      <th scope="row">3</th>
                      <td>Total Deals:-</td>
                      <td>{this.state.totalDeal}</td>


                    </tr>

                  </tbody>
                </Table>

              </CardBody>

            </Card>
          </Col> */}
          {/* <Col sm="12" lg="4" md="4">
            <Card className="shadow">
              <CardHeader className="bg-dark text-white" >
                Restaurant Counts
              </CardHeader>
              <CardBody >
                <Table >
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Title</th>
                      <th>Count</th>

                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <th scope="row">1</th>
                      <td>Active Restaurant:-</td>
                      <td>{this.state.activeResturant}</td>

                    </tr>
                    <tr>
                      <th scope="row">2</th>
                      <td>Inactive Restaurant:-</td>
                      <td>{this.state.inActiveResturants}</td>


                    </tr>
                    <tr>
                      <th scope="row">3</th>
                      <td>Total Restaurant:-</td>
                      <td>{this.state.totalResturant}</td>


                    </tr>

                  </tbody>
                </Table>

              </CardBody>

            </Card>
          </Col> */}
        </Row>
      </div>
    );
  }
}

export default Dashboard;
