export default {
  items: [
    {
      name: 'Dashboard',
      url: '/admin/dashboard',
      icon: 'icon-speedometer',
    },
    {
      name: 'Users',
      url: '/admin/users',
      icon: 'icon-people',
    },
    {
      name: 'Watches',
      url: '/admin/watches',
      icon: 'icon-people',
    },
    {
      name: 'Error Report',
      url: '/admin/errorreport',
      icon: 'icon-people',
    },
    // {
    //   name: 'Deals Type',
    //   url: '/admin/dealsType',
    //   icon: 'icon-diamond',
    // },
    // {
    //   name: 'Intention',
    //   url: '/admin/intention',
    //   icon: 'icon-pie-chart',
    // },
    // // {
    // //   name: 'Redemption',
    // //   url: '/admin/redemption',
    // //   icon: 'icon-pie-chart',
    // // },
    // {
    //   name: 'Support',
    //   url: '/admin/support',
    //   icon: 'icon-envelope',
    // },
    // {
    //   name: 'Settings',
    //   url: '/admin/setting',
    //   icon: 'icon-wrench',
    // },
  ],
};
