const routes = {
  '/admin': 'Home',
  '/admin/dashboard': 'Dashboard',
  '/admin/restaurants': 'Restaurants',
  '/admin/customers': 'Customers',
  '/admin/deals': 'Deals',
  '/admin/dealsType': 'Deals Type',
  '/admin/support': 'Support',
  '/admin/intention': 'Intention',
  '/admin/setting': 'Settings',
  '/admin/redemption': 'Redemption',
};
export default routes;
