import React from 'react';
// import PropTypes from 'prop-types';
import { Container } from 'reactstrap';

import Header from './common/Header';
import Sidebar from './common/Sidebar';
import Breadcrumb from './common/Breadcrumb';


class DashLayout extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
  }

  render() {
    return (
      <div className="app">
        <Header {...this.props} />
        <div className="app-body">
          <Sidebar {...this.props} />
          <main className="main">
            <Breadcrumb />
            <Container fluid>
              {/* <WrappedComponent {...this.props} /> */}
              {React.cloneElement(this.props.children, { ...this.props })}
            </Container>
          </main>
        </div>
      </div>
    );
  }
}

export default DashLayout;
