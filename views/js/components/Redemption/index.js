import React, { Component } from 'react';
import {
  Row, Col, Card, CardHeader, CardBody, FormGroup,
  Form,
  Label,
  Input,
  Button,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Collapse,
} from 'reactstrap';
import Pagination from 'react-js-pagination';
import { toasterMessage } from '../../utils/toaster';
import proxy from '../../utils/proxy';
import Loader from '../../Common/Loader/Index';
import CustomTable from '../../Common/Table';


export default class index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      redemptionList: '',
      showLoader: false,
      pagination: {
        activePage: 1,
        itemsCountPerPage: 10,
        pageRangeDisplayed: 5,
      },
    };
    this.handlePageChange = this.handlePageChange.bind(this);
  }
  componentWillMount() {
    this.getRedemption();
  }
  getRedemption() {
    if (localStorage.getItem('token')) {
      this.setState({ showLoader: true });
      proxy.call('get', 'redemption', null, null, localStorage.getItem('token'))
        .then((response) => {
          console.log(response.data, 'response');
          if (response &&
            (response.status === 200 || response.status === 201) &&
            response.data &&
            response.data
          ) {
            this.setState({
              ...this.state,
              redemptionList: response.data.data,
              showLoader: false,
            }, () => console.log(this.state.redemptionList, 'frm setstate'));

          }
        })
        .catch((err) => {
          // this.setState({ showLoader: false });
          toasterMessage(
            'error',
            (
              err
              && err.response
              && err.response.data
              && err.response.data.message
            ) || (
              err
              && err.message
            ),
          );
        });

    } else { this.props.history.push('/'); }
  }
  handlePageChange(pageNumber) {
    this.setState({
      ...this.state, pagination: { activePage: pageNumber },
    }, () => this.getRedemption());
  }
  tableData() {
    return {
      labels: [{
        label: 'Customer Name',
        value: 'customerName',
        isDate: false,
      }, {
        label: 'Reference No',
        value: 'restuarantName',
        isDate: false,
      },
      //  {
      //   label: 'Contact Number',
      //   value: 'contactNumber',
      //   isDate: false,
      // },
      {
        label: 'No Of Paterns',
        value: 'PeopleNumber',
        isDate: false,
      },
      {
        label: 'Claimed Date',
        value: 'dealTitle',
        isDate: false,
      },

      {
        label: 'Claimed Time',
        value: 'claimedTime',
        isDate: true,
      },
      {
        label: 'Status',
        value: 'status',
        isDate: false,
      }],
      content: (
        this.state.redemptionList
        && this.state.redemptionList.length
        && this.state.redemptionList.map(customer => ({
          ...customer,
          restuarantName: (
            customer.RestroData.map(item => item.name)
          ),
          dealTitle: (
            customer.dealsData.map(item => item.dealTitle)
          ),
          customerName: (
            customer.userData.name
          ),
        }))
      ) || null,
    };
  }
  render() {
    return (
      <div>
        {this.state.showLoader ? <Loader /> : null}
        <Card>
          <CardHeader className="bg-dark text-white" >
            Redemptions
          </CardHeader>
          <CardBody>
            <CustomTable
              data={this.tableData()}
            />
          </CardBody>
        </Card>

        <Pagination
          activePage={this.state.pagination.activePage}

          totalItemsCount={this.state.pagination.totalItemsCount}
          pageRangeDisplayed={5}
          onChange={this.handlePageChange}
        />
      </div>
    );
  }
}
