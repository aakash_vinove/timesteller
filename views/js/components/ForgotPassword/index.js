/*eslint-disable */
import React, { Component } from 'react';
import {
  Container,
  Row,
  Col,
  CardGroup,
  Card,
  CardBody,
  Button,
  Input,
  InputGroup,
  InputGroupAddon,
  Form,
} from 'reactstrap';

// import local files
import proxy from '../../utils/proxy';
import { toasterMessage } from '../../utils/toaster';
import { emailRegex, passwordRegex } from '../../utils/config';

class ForgotPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      pass: '',
      cpass: '',
      otp: '',
      stage: 1,
      error: {},
    };
    this.handleSubmitEmail = this.handleSubmitEmail.bind(this);
    this.handleSubmitOtp = this.handleSubmitOtp.bind(this);
    this.handleSubmitPassword = this.handleSubmitPassword.bind(this);
  }
  componentDidMount() {
    if (localStorage.getItem('token')) {
      this.props.history.push('/admin/dashboard');
    }
  }

  handleSubmitEmail(event) {
    event.preventDefault();
    const { email, error } = this.state;
    if (!email) {
      error.email = 'Email Required';
    } else if (!emailRegex.test(email)) {
      error.email = 'Invalid email address';
    } else {
      delete error.email;
    }
    this.setState({
      error
    });
    if (JSON.stringify(error) !== '{}') {
      return null;
    }
    if (email) {
      proxy.call('post', 'forgotPasswordAPI', null, { email })
        .then((response) => {
            console.log("hii")
            toasterMessage('success', 'Please check your Mail id');
            this.props.history.push('/')
          
        })
        .catch((err) => {
          console.group(':: err ', err)
          toasterMessage('error', err.response.data.message)
        })
    }
  }

  handleSubmitOtp(event) {
    event.preventDefault();
    const { otp, error } = this.state;
    if (!otp) {
      error.otp = 'OTP Required';
    } else {
      delete error.otp;
    }
    this.setState({
      error
    });
    if (JSON.stringify(error) !== '{}') {
      return null;
    }

    if (otp) {
      proxy.call('post', 'resetPassword', null, { generateOTP: otp })
        .then((response) => {
          console.log(':: response ', response)
          if (response && response.status === 200) {
            this.setState({
              stage: 3
            });
          }
        })
        .catch((err) => {
          console.group(':: err ', err)
          toasterMessage('error', err.response.data.message)
        })
    }
  }

  handleSubmitPassword(event) {
    event.preventDefault();
    const {
      cpass,
      email,
      pass,
      error
    } = this.state;
    if (!pass) {
      error.pass = 'New Password Required';
    } else if (!passwordRegex.test(pass)) {
      error.pass = '*Password must be 8-13 characters long, must have 1 lowercase, 1 uppercase, 1 number & a special character eg. !@#$%^&.';
    } else {
      delete error.pass;
    }
    if (!cpass) {
      error.cpass = 'Confirm Password Required';
    } else if (pass !== cpass) {
      error.cpass = 'New Password and Confirm Password must be same';
    } else {
      delete error.cpass;
    }
    this.setState({
      error
    });
    if (JSON.stringify(error) !== '{}') {
      return null;
    }
    if (email && pass) {
      proxy.call('post', 'updatePassword', null, { email, password: pass })
        .then((response) => {
          console.log(':: response ', response)
          if (response && (response.status === 200 || response.status === 201)) {
            toasterMessage('success', 'Password reset successful');
            this.props.history.push('/')
          }
        })
        .catch((err) => {
          console.group(':: err ', err)
          toasterMessage('error', err.response.data.message)
        })
    }
  }

  renderEmailPart() {
    return (
      <Form onSubmit={this.handleSubmitEmail}>
        <p className="text-muted">Enter email to reset the password</p>
        <div>
          {
            this.state.error
            && this.state.error.email
            && (<span className="input-error">{this.state.error.email}</span>)
          }
          <InputGroup className="mb-3">
            <InputGroupAddon addonType="prepend"><i className="icon-user" /></InputGroupAddon>
            <Input
              type="email"
              value={this.state.email}
              onChange={e => this.setState({ email: e.target.value })}
              placeholder="Email"
            />
          </InputGroup>
        </div>
        <Row>
          <Col xs="6">
            <Button color="primary" type="submit" className="px-4">Submit</Button>
          </Col>
          <Col xs="6" className="text-right">
            <Button color="link" className="px-0" onClick={() => { this.props.history.push('/') }}>
              Back to Login
            </Button>
          </Col>
        </Row>
      </Form>
    )
  }

  renderOTPPart() {
    return (
      <Form onSubmit={this.handleSubmitOtp}>
        <p className="text-muted">Enter OTP to reset the password</p>
        <div>
          {
            this.state.error
            && this.state.error.otp
            && (<span className="input-error">{this.state.error.otp}</span>)
          }
          <InputGroup className="mb-3">
            <InputGroupAddon addonType="prepend"><i className="icon-user" /></InputGroupAddon>
            <Input
              type="text"
              value={this.state.otp}
              onChange={e => this.setState({ otp: e.target.value })}
              placeholder="OTP"
            />
          </InputGroup>
        </div>
        <Row>
          <Col xs="6">
            <Button color="primary" type="submit" className="px-4">Submit</Button>
          </Col>
          <Col xs="6" className="text-right">
            <Button color="link" className="px-0" onClick={() => { this.props.history.push('/') }}>
              Back to Login
            </Button>
          </Col>
        </Row>
      </Form>
    )
  }

  renderPasswordPart() {
    return (
      <Form onSubmit={this.handleSubmitPassword}>
        <p className="text-muted">Enter new password</p>
        <div>
          {
            this.state.error
            && this.state.error.pass
            && (<span className="input-error">{this.state.error.pass}</span>)
          }
          <InputGroup className="mb-3">
            <InputGroupAddon addonType="prepend"><i className="icon-lock" /></InputGroupAddon>
            <Input
              type="password"
              value={this.state.pass}
              onChange={e => this.setState({ pass: e.target.value })}
              placeholder="New Password"
            />
          </InputGroup>
        </div>
        <div>
          {
            this.state.error
            && this.state.error.cpass
            && (<span className="input-error">{this.state.error.cpass}</span>)
          }
          <InputGroup className="mb-3">
            <InputGroupAddon addonType="prepend"><i className="icon-lock" /></InputGroupAddon>
            <Input
              type="password"
              value={this.state.cpass}
              onChange={e => this.setState({ cpass: e.target.value })}
              placeholder="Confirm Password"
            />
          </InputGroup>
        </div>
        <Row>
          <Col xs="6">
            <Button color="primary" type="submit" className="px-4">Submit</Button>
          </Col>
          <Col xs="6" className="text-right">
            <Button color="link" className="px-0" onClick={() => { this.props.history.push('/') }}>
              Back to Login
            </Button>
          </Col>
        </Row>
      </Form>
    )
  }

  render() {
    return (
      <div className="app flex-row align-items-center">
        <Container>
          <Row className="justify-content-center">
            <Col md="8">
              <CardGroup className="mb-0">
                <Card className="p-4">
                  <CardBody className="card-body">
                    <h1>Forgot Password</h1>
                    {
                      this.state.stage === 1 ?
                        this.renderEmailPart()
                        :
                        this.state.stage === 2 ?
                          this.renderOTPPart()
                          :
                          this.renderPasswordPart()
                    }
                  </CardBody>
                </Card>
              </CardGroup>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default ForgotPassword;
