/*eslint-disable */
import React, { Component } from 'react';
import { TabContent, Modal, ModalHeader, ModalBody, ModalFooter, TabPane, Nav, NavItem, NavLink, Card, Button, CardTitle, CardText, Row, Col, Input, Form, FormGroup, Label, FormText, CardBody, CardHeader } from 'reactstrap';
import classnames from 'classnames';
import CustomTable from '../../Common/Table';
import { toasterMessage } from '../../utils/toaster';
import proxy from '../../utils/proxy';
import Loader from '../../Common/Loader/Index';

export default class index extends Component {
  constructor(props) {
    super(props);

    this.imageRef = React.createRef();
    this.state = {
      activeTab: '1',
      heading: 'Add New',
      currentPass: '',
      newPass: '',
      confirmPass: '',
      error: {},
      showLoader: false,
      image: null,
      imagePreview: '',
      categoryName: null,
      rewardList: '',
      fromRange: '',
      categoryList: '',
      classificationList: '',
      toRange: '',
      categoryId: '',
      classificationId: '',
      baseUrl: 'http://realtimedeals.n1.iworklab.com:3005/',
    };
    this.changePassword = this.changePassword.bind(this);
    this.checkPasswordValidation = this.checkPasswordValidation.bind(this);
    this.imageChange = this.imageChange.bind(this);
    this.addReward = this.addReward.bind(this);
    this.getReward = this.getReward.bind(this);
    this.toggle = this.toggle.bind(this);
    this.deleteReward = this.deleteReward.bind(this);
    this.checkRewardValidation = this.checkRewardValidation.bind(this);
    this.checkCategoryValidation = this.checkCategoryValidation.bind(this);
    this.addCategory = this.addCategory.bind(this);
    this.removeClassification = this.removeClassification.bind(this);
    this.deleteClassification = this.deleteClassification.bind(this);
    this.toggledel = this.toggledel.bind(this);
    this.selectCategory = this.selectCategory.bind(this);
    this.updateCategory = this.updateCategory.bind(this);
    // this.selectCategory = this.selectCategory.bind(this);
    // this.categoryTable = this.toggle.bind(this);
  }
  componentDidMount() {
    this.getCategoryList();

    this.getclassification();
    // this.getReward();
  }
  getclassification() {

    this.toggle('1');
    if (localStorage.getItem('token')) {
      this.setState({ showLoader: true });
      proxy.call('get', 'rewardRangeList', null, null, localStorage.getItem('token'))
        .then((response) => {
          console.log(response, 'rewardCategoryList');
          if (
            response
            && (
              response.status === 200
              || response.status === 201
            )
            && response.data


          ) {

            this.setState({ showLoader: false, classificationList: response.data.data });


          }
        })
        .catch((err) => {
          this.setState({ showLoader: false });
          toasterMessage(
            'error',
            (
              err
              && err.response
              && err.response.data

            ) || (
              err
              && err.message
            ),
          );
        });

    } else {

      this.props.history.push('/');
    }

  }
  getCategoryList() {
    if (localStorage.getItem('token')) {
      this.setState({ showLoader: true });
      proxy.call('get', 'rewardCategoryList', null, null, localStorage.getItem('token'))
        .then((response) => {
          console.log(response, 'rewardCategoryList');
          if (
            response
            && (
              response.status === 200
              || response.status === 201
            )
            && response.data


          ) {

            this.setState({ showLoader: false, categoryList: response.data.data });


          }
        })
        .catch((err) => {
          this.setState({ showLoader: false });
          toasterMessage(
            'error',
            (
              err
              && err.response
              && err.response.data

            ) || (
              err
              && err.message
            ),
          );
        });

    } else {

      this.props.history.push('/');
    }
  }
  getReward() {

    this.toggle('2');
    if (localStorage.getItem('token')) {
      this.setState({ showLoader: true });
      proxy.call('get', 'rewardCategory', null, null, localStorage.getItem('token'))
        .then((response) => {
          console.log(response, 'delrespo');
          if (
            response
            && (
              response.status === 200
              || response.status === 201
            )
            && response.data


          ) {

            this.setState({ showLoader: false, rewardList: response.data.data });


          }
        })
        .catch((err) => {
          this.setState({ showLoader: false });
          toasterMessage(
            'error',
            (
              err
              && err.response
              && err.response.data

            ) || (
              err
              && err.message
            ),
          );
        });

    } else {

      this.props.history.push('/');
    }
  }
  checkPasswordValidation(e) {
    e.preventDefault();
    const {
      currentPass,
      newPass,
      confirmPass, error,
    } = this.state;
    if (!currentPass) {
      error.currentpass = 'current password required';
    } else {
      delete error.currentpass;
    }
    if (!newPass) {
      error.newPass = 'can\'t be empty';
    } else if (newPass.length < 6) {
      error.newPass = 'password length should be atleast 6 char long';
    } else {
      delete error.newPass;
    }
    if (newPass !== confirmPass) {
      error.samepass = 'new password and confirm password should be same';
    } else {
      delete error.samepass;
    }
    if (!confirmPass) {
      error.confirmpass = 'can\'t be empty';
    } else {
      delete error.confirmpass;
    }
    this.setState({
      error,
    });
    console.log(this.state.error, 'error');
    if (JSON.stringify(error) !== '{}') {
      return null;
    }
    if (newPass && confirmPass && currentPass) {
      this.changePassword();
    }
    console.log(this.state.error, 'error');
  }
  changePassword() {
    const body = {
      password: this.state.currentPass,
      newPassword: this.state.newPass,
    };
    console.log(body, 'boidyyy');
    // e.preventDefault();
    if (localStorage.getItem('token')) {
      this.setState({ showLoader: true });
      proxy.call('post', 'changePasswordAdmin', null, body, localStorage.getItem('token'))
        .then((response) => {
          console.log(response, 'delrespo');


          if (
            response
            && (
              response.status === 200
              || response.status === 201
            )
            && response.data


          ) {
            console.log('frm im');
            toasterMessage(
              'success',
              (
                response
                && response.data

                && response.data.message
              ) || (
                response
                && response.message
              ),
            );
            this.setState({
              showLoader: false,
              currentPass: '',
              newPass: '',
              confirmPass: '',
            });


          }

        })
        .catch((err) => {
          this.setState({ showLoader: false });
          console.log('atatatat-- ', err.response.data.message);
          // this.setState({ showLoader: false });
          toasterMessage(
            'error',
            (
              err
              && err.response
              && err.response.data.message

            ) || (
              err
              && err.response.data.message
            ),
          );
        });

    } else {

      this.props.history.push('/');
    }
  }
  imageChange(event) {
    console.log(event, 'event');
    this.setState({
      image: (event.target.files[0]),
      imagePreview: URL.createObjectURL(event.target.files[0]),
    }, () => console.log(this.state.imagePreview), 'imageset');
  }
  checkRewardValidation() {
    console.log('working');
    const { categoryName, imagePreview, error } = this.state;
    if (!categoryName) {
      error.categoryName = 'title required';
    } else {
      delete error.categoryName;
    }
    if (!imagePreview) {
      error.imagePreview = 'image required';
    } else {
      delete error.imagePreview;
    }
    this.setState({
      error,
    });
    if (JSON.stringify(error) !== '{}') {
      return null;
    }
    if (categoryName && imagePreview) {
      this.addReward();
    }
  }
  addReward() {
    const body = {
      name: this.state.categoryName,
      categoryImage: this.state.image,
    };
    const form = new FormData();
    for (const key in body) {
      form.append(key, body[key]);
    }
    if (localStorage.getItem('token')) {
      this.setState({ showLoader: true });
      proxy.call('post', 'rewardCategory', null, form, localStorage.getItem('token'))
        .then((response) => {
          console.log(response, 'delrespo');
          if (
            response
            && (
              response.status === 200
              || response.status === 201
            )
            && response.data


          ) {
            this.getReward();
            console.log('frm im');
            toasterMessage(
              'success',
              (
                response
                && response.data

                && response.data.message
              ) || (
                response
                && response.message
              ),
            );
            this.imageRef.current.value = null;
            this.setState({

              showLoader: false,
              image: '',
              imagePreview: '',
              categoryName: '',
            }, () => console.log(this.state, 'states'));

          }
        })
        .catch((err) => {
          this.setState({
            ...this.state, showLoader: false, image: '', imagePreview: '',
          });
          toasterMessage(
            'error',
            (
              err
              && err.response
              && err.response.data

            ) || (
              err
              && err.message
            ),
          );
        });

    } else {

      this.props.history.push('/');
    }
  }

  deleteReward(item) {

    const id = item._id;
    const Id = { id };
    if (localStorage.getItem('token')) {
      this.setState({ showLoader: true });
      proxy.call('delete', 'categoryDelete', Id, null, localStorage.getItem('token'))
        .then((response) => {
          console.log(response, 'delrespo');
          if (
            response
            && (
              response.status === 200
              || response.status === 201
            )
            && response.data


          ) {
            console.log('frm im');
            toasterMessage(
              'success',
              (
                response
                && response.data

                && response.data.message
              ) || (
                response
                && response.message
              ),
            );
            this.getReward();

            this.setState({
              ...this.state,
              image: '',
              imagePreview: '',
              showLoader: false,

            });
          }
        })
        .catch((err) => {
          // this.setState({ showLoader: false });
          toasterMessage(
            'error',
            (
              err
              && err.response
              && err.response.data.message

            ) || (
              err
              && err.message
            ),
          );
        });

    } else {

      this.props.history.push('/');
    }
    console.log(item);
  }
  checkCategoryValidation() {
    const { error, categoryId } = this.state;
    let {
      fromRange, toRange,
    } = this.state;
    console.log('gbfgbfd', this.state);
    fromRange = Number(fromRange);
    toRange = Number(toRange);


    if (!fromRange) {
      error.fromRange = 'field required';
    } else if (fromRange < 0) {
      error.fromRange = 'enter a valid number';
    } else {
      delete error.fromRange;
    }
    if (!toRange) {
      error.toRange = 'field required';
    } else if (toRange < 0) {
      error.toRange = 'enter a valid number';
    } else {
      delete error.toRange;
    }
    if (!categoryId) {
      error.selectCategory = 'please select category';
    } else {
      delete error.selectCategory;
    }
    if (fromRange >= toRange) {
      console.log('ssdsd ');
      error.invalidRange = '  ';
      alert('enter valid values eg:-from value < to value');
      return;
    }

    delete error.invalidRange;


    this.setState({
      error,
    });

    console.log(this.state.error, 'error');
    if (JSON.stringify(error) !== '{}') {
      return null;
    }
    if (fromRange && toRange && categoryId) {
      if (this.state.heading === 'Add New') { console.log('updtae', this.state.heading); this.addCategory(); } else {
        console.log('add', this.state.heading);
        this.updateCategory();
      }
    }
    console.log(this.state.error, 'error');
  }
  updateCategory() {
    const id = this.state.classificationId;
    const Id = { id };


    this.setState({ showLoader: true });
    console.log(body, 'boidyyy', Id);
    const body = {
      from: this.state.fromRange,
      to: this.state.toRange,
      categoryId: this.state.categoryId,
    };
    if (localStorage.getItem('token')) {
      this.setState({ showLoader: true });
      proxy.call('put', 'rewardRangeEdit', Id, body, localStorage.getItem('token'))
        .then((response) => {
          console.log(response, 'delrespo');
          if (
            response
            && (
              response.status === 200
              || response.status === 201
            )
            && response.data


          ) {
            this.getclassification();
            this.getCategoryList();
            console.log('frm im');
            toasterMessage(
              'success',
              (
                response
                && response.data

                && response.data.message
              ) || (
                response
                && response.message
              ),
            );
            this.imageRef.current.value = null;
            // window.location.reload();
            this.setState({
              ...this.state,
              showLoader: false,
              fromRange: '',
              toRange: '',
              categoryId: '',
            }, () => console.log(this.state, 'states'));

          }
        })
        .catch((err) => {
          this.setState({
            ...this.state, showLoader: false, image: '', imagePreview: '',
          });
          toasterMessage(
            'error',
            (
              err
              && err.response
              && err.response.data

            ) || (
              err
              && err.message
            ),
          );
        });

    } else {

      this.props.history.push('/');
    }
  }
  addCategory() {

    const body = {
      from: this.state.fromRange,
      to: this.state.toRange,
      categoryId: this.state.categoryId,
    };
    this.setState({ showLoader: true });
    console.log(body, 'boidyyy');

    if (localStorage.getItem('token')) {
      this.setState({ showLoader: true });
      proxy.call('post', 'rewardRange', null, body, localStorage.getItem('token'))
        .then((response) => {
          console.log(response, 'delrespo');
          if (
            response
            && (
              response.status === 200
              || response.status === 201
            )
            && response.data


          ) {
            this.getclassification();
            this.getCategoryList();
            console.log('frm im');
            toasterMessage(
              'success',
              (
                response
                && response.data

                && response.data.message
              ) || (
                response
                && response.message
              ),
            );
            this.imageRef.current.value = null;
            window.location.reload();
            this.setState({
              ...this.state,
              showLoader: false,
              fromRange: '',
              toRange: '',
              categoryId: '',
            }, () => console.log(this.state, 'states'));

          }
        })
        .catch((err) => {
          this.setState({
            ...this.state, showLoader: false, image: '', imagePreview: '',
          });
          toasterMessage(
            'error',
            (
              err
              && err.response
              && err.response.data

            ) || (
              err
              && err.message
            ),
          );
        });

    } else {

      this.props.history.push('/');
    }
  }

  toggle(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab,
      });
    }
  }
  rewardTable() {
    return {
      labels: [{
        label: 'Title',
        value: 'categoryName',
        isDate: false,
      },
      {
        label: 'Created At',
        value: 'createdAt',
        isDate: true,
      },
      {
        label: 'Icon',
        value: 'icon',
        isDate: false,
      },
      {
        label: 'Action',
        value: 'action',
        isDate: false,
      }],
      content: (
        this.state.rewardList
        && this.state.rewardList.length
        && this.state.rewardList.map(deal => ({
          ...deal,
          icon: (
            <div style={{ textAlign: 'center' }}>
              <span
                className="action-icon edit-icon"
              >
                <a target="_blank " href={this.state.baseUrl + deal.categoryImage}>
                  <img style={{ height: '40px', width: '40px' }} src={this.state.baseUrl + deal.categoryImage} alt="icon" />
                </a>
                {/* <img style={{ height: '40px', width: '40px' }} src={this.state.baseUrl + deal.categoryImage} alt="icon" /> */}
              </span>
            </div>

          ),
          action: (
            <div style={{ textAlign: 'center' }}>

              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <span
                className="action-icon remove-icon"
                onClick={() => { this.deleteReward(deal); }}
              >
                <i className="icon-trash" />
              </span>
            </div>
          ),

        }))
      ),
    };
  }
  categoryTable() {
    return {
      labels: [{
        label: 'Range',
        value: 'range',
        isDate: false,
      },
      {
        label: 'Category',
        value: 'categoryName',
        isDate: false,
      },
      {
        label: 'Created At',
        value: 'createdAt',
        isDate: true,
      },
      {
        label: 'Updated At',
        value: 'updatedAt',
        isDate: true,
      }, {
        label: 'Action',
        value: 'action',
        isDate: false,
      }],

      content: (
        this.state.classificationList
        && this.state.classificationList.length
        && this.state.classificationList.map(deal => ({
          ...deal.categoryId,
          range: (
            <div style={{ textAlign: 'center' }}>{deal.from + '-' + deal.to}</div>
          ),

          action: (
            <div style={{ textAlign: 'center' }}>
              <span
                className="action-icon edit-icon"
                onClick={() => { this.editClassification(deal); }}
              >
                <i className="icon-pencil" />
              </span>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <span
                className="action-icon remove-icon"
                onClick={() => { this.removeClassification(deal); }}
              >
                <i className="icon-trash" />
              </span>
            </div>
          ),
        }))
      ) || null,
    };
  }
  editClassification(item) {

    console.log('item', item);
    this.setState({
      heading: 'Edit',
      fromRange: item.from,
      toRange: item.to,
      classificationId: item._id,
      categoryId: item.categoryId._id,
    }, () => console.log(this.state, 'statwa'));

  }
  toggledel() {
    this.setState(prevState => ({
      delModal: !prevState.delModal,
    }));
  }
  removeClassification(item) {
    this.setState({ classificationId: item._id });
    this.toggledel();
  }
  deleteClassification() {
    this.setState({ showLoader: true });
    this.toggledel();
    // console.log(item.categoryId._id, 'frm delet');
    const id = this.state.classificationId;
    const Id = { id };
    if (localStorage.getItem('token')) {
      this.setState({ showLoader: true });
      proxy.call('delete', 'rewardRangeDelete', Id, null, localStorage.getItem('token'))
        .then((response) => {
          console.log(response, 'delrespo');
          if (
            response
            && (
              response.status === 200
              || response.status === 201
            )
            && response.data


          ) {
            console.log('frm im');
            toasterMessage(
              'success',
              (
                response
                && response.data

                && response.data.message
              ) || (
                response
                && response.message
              ),
            );
            this.getclassification();

            this.setState({
              ...this.state,
              image: '',
              imagePreview: '',
              showLoader: false,

            });
          }
        })
        .catch((err) => {
          this.setState({ showLoader: false });
          toasterMessage(
            'error',
            (
              err
              && err.response
              && err.response.data.message

            ) || (
              err
              && err.message
            ),
          );
        });

    } else {

      this.props.history.push('/');
    }
    console.log(item);
  }

  selectCategory(event) {
    this.setState({
      categoryId: event.target.value,
    }, () => console.log('state', this.state));
  }

  render() {
    console.log('from render');
    return (
      <div>
        {this.state.showLoader ? <Loader /> : ''}
        {/* delete modal starts */}
        <Modal
          style={{ width: '25rem' }}
          isOpen={this.state.delModal}
          toggle={this.toggledel}
          className={this.props.className}
        >
          <ModalHeader toggle={this.toggledel}>Delete</ModalHeader>
          <ModalBody>Are you sure want to Delete ?  Once deleted can't be retreived </ModalBody>
          <ModalFooter>
            <Button color="danger" onClick={this.deleteClassification}>
              Delete
            </Button>
            <Button color="secondary" onClick={this.toggledel}>
              Cancel
            </Button>
          </ModalFooter>
        </Modal>
        {/* delet modal ends */}
        <Nav tabs>
          <NavItem>
            <NavLink
              className={classnames({ active: this.state.activeTab === '1' })}
              onClick={() => { this.getclassification(); }}
            >
              Categories Classification
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              className={classnames({ active: this.state.activeTab === '2' })}
              onClick={(e) => { this.getReward(e); }}
            >
              Reward Categories
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              className={classnames({ active: this.state.activeTab === '3' })}
              onClick={() => { this.toggle('3'); }}
            >
              Profile
            </NavLink>
          </NavItem>

        </Nav>
        <TabContent activeTab={this.state.activeTab}>
          <TabPane tabId="1">
            <Row>
              <Col sm="6">
                <Card >
                  <CardHeader className="bg-dark text-light">{this.state.heading}</CardHeader>
                  <CardBody>
                    <Form>
                      <Row className="justify-content-center mb-4">
                        <Col md="5">
                          <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                            <Label for="restaurantName" className="mr-sm-2">
                              Enter range
                            </Label>

                            <Row>

                              <Col md="5">

                                <Input value={this.state.fromRange} onChange={e => this.setState({ fromRange: e.target.value })} type="number" />
                                <Label>From</Label>
                                <br />

                              </Col>
                              <Col md="1">-</Col>

                              <Col md="5">  <Input value={this.state.toRange} onChange={e => this.setState({ toRange: e.target.value })} type="number" />
                                <Label>to</Label>
                                <br />

                              </Col>
                            </Row>
                            <Row>
                              <Col md="6">  {
                                this.state.error
                                && this.state.error.fromRange
                                && (<span className="input-error">{this.state.error.fromRange}</span>)
                              }
                              </Col>
                              <Col md="6">    {
                                this.state.error
                                && this.state.error.toRange
                                && (<span className="input-error">{this.state.error.toRange}</span>)
                              }
                              </Col>
                            </Row>

                          </FormGroup>

                        </Col>
                        <Col md="5">
                          <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                            <Label for="restaurantName" className="mr-sm-2">
                              Select Category
                            </Label>

                            <Input value={this.state.categoryId} onChange={e => this.selectCategory(e)} type="select" >
                              <option value=" ">Please Select</option>
                              {this.state.categoryList ? this.state.categoryList.map(item => (
                                <option value={item._id} >{item.categoryName}</option>
                              )) : null}

                            </Input>
                            {
                              this.state.error
                              && this.state.error.selectCategory
                              && (<span className="input-error">{this.state.error.selectCategory}</span>)
                            }
                          </FormGroup>
                        </Col>

                      </Row>
                      <Row className="justify-content-center mb-4">
                        {/* {
                          this.state.error
                            && this.state.error.invalidRange
                            && (<span className="input-error">{this.state.error.invalidRange}</span>)
                        } */}

                        <Button onClick={this.checkCategoryValidation} >Save</Button>
                      </Row>
                    </Form>
                  </CardBody>
                </Card>
              </Col>
              <Col sm="6">
                <Card >
                  <CardHeader className="bg-dark text-light">All Categories</CardHeader>
                  <CardBody>
                    <CustomTable data={this.categoryTable()} />
                  </CardBody>
                </Card>
              </Col>
            </Row>
          </TabPane>
          <TabPane tabId="2">
            <Row>
              <Col sm="6">
                <Card >
                  <CardHeader className="bg-dark text-light">{this.state.heading}</CardHeader>
                  <CardBody>
                    <Form>
                      <Row className="justify-content-center mb-4">
                        <Col md="3">
                          <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                            <Label for="restaurantName" className="mr-sm-2">
                              Category Name
                            </Label>

                            <Input type="text" value={this.state.categoryName} onChange={e => this.setState({ categoryName: e.target.value })} />
                            {
                              this.state.error
                              && this.state.error.categoryName
                              && (<span className="input-error">{this.state.error.categoryName}</span>)
                            }
                          </FormGroup>

                        </Col>
                        <Col md="3">
                          <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                            <Label for="restaurantName" className="mr-sm-2">
                              Category Image
                            </Label>

                            <input type="file" ref={this.imageRef} onChange={e => this.imageChange(e)} accept="image/png, .jpeg, .jpg, image/gif" required />
                            {
                              this.state.error
                              && this.state.error.imagePreview
                              && (<span className="input-error">{this.state.error.imagePreview}</span>)
                            }
                          </FormGroup>
                        </Col>
                        <Col md="4" />
                        <Col md="2">
                          <img style={{ height: '70px', width: '70px' }} src={this.state.imagePreview} alt="" />
                        </Col>

                      </Row>
                      <Row className="justify-content-center mb-4">
                        <Button onClick={this.checkRewardValidation}> Save</Button>
                      </Row>
                    </Form>
                  </CardBody>
                </Card>
              </Col>
              <Col sm="6">
                <Card >
                  <CardHeader className="bg-dark text-light">All Categories</CardHeader>
                  <CardBody>
                    <CustomTable data={this.rewardTable()} />
                  </CardBody>
                </Card>
              </Col>
            </Row>
          </TabPane>
          <TabPane tabId="3">
            <Card>
              <Row>
                <Col sm="12">
                  <CardHeader className="bg-dark text-white">Change Password</CardHeader>
                  <Form className="mt-4 p-3">
                    <Row form>
                      <Col md={3}>
                        <FormGroup>
                          <Label for="exampleEmail">Current Password</Label>
                          <Input value={this.state.currentPass} onChange={(e) => { this.setState({ currentPass: e.target.value }); }} type="password" name="currentpassword" id="exampleEmail" placeholder="enter current password" />
                          {
                            this.state.error
                            && this.state.error.currentpass
                            && (<span className="input-error">{this.state.error.currentpass}</span>)
                          }
                        </FormGroup>
                      </Col>
                      <Col md={3}>
                        <FormGroup>
                          <Label for="exampleEmail">New password</Label>
                          <Input onChange={(e) => { this.setState({ newPass: e.target.value }); }} value={this.state.newPass} type="password" name="newpassword" id="exampleEmail" placeholder="enter new password" />
                          {
                            this.state.error
                            && this.state.error.newPass
                            && (<span className="input-error">{this.state.error.newPass}</span>)
                          }
                        </FormGroup>

                      </Col>
                      <Col md={3}>
                        <FormGroup>
                          <Label for="examplePassword">Confirm Password</Label>
                          <Input value={this.state.confirmPass} onChange={(e) => { this.setState({ confirmPass: e.target.value }); }} type="password" name="confirmpassword" id="examplePassword" placeholder="confirm new password " />
                          {
                            this.state.error
                            && this.state.error.confirmpass
                            && (<span className="input-error">{this.state.error.confirmpass}</span>)
                          }
                        </FormGroup>
                      </Col>

                    </Row>
                    <Row className="mt-2 mb-2 justify-content-center"> {
                      this.state.error
                      && this.state.error.samepass
                      && (<span className="input-error">{this.state.error.samepass}</span>)
                    }
                    </Row>

                    <Button onClick={e => this.checkPasswordValidation(e)} >
                      Update
                    </Button>
                  </Form>
                </Col>
              </Row>
            </Card>

          </TabPane>
        </TabContent>
      </div>
    );
  }
}
