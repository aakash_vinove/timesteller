import React from 'react';
import './style.css';

export default function index() {
  return (
    <div>
      <nav className="navbar navbar-dark bg-dark">
        <a className="navbar-brand" href="#">Navbar</a>
      </nav>

      <div className="container mt-5" >
        <h1 className="newspaper-title">Newspaper Title</h1>
        <article className="main-article shadow">
          <h3 className="title">Article Title</h3>
          <h4 className="author">Author's Name</h4>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
        </article>
        <div className="row">
          <div className="col">

            <div id="comments" className="comments-wrapper">
              <h2>
                {/* <span v-if="comments.length === 0">Be the first to comment!</span> */}
                <span >  Comments</span>

                {/* <a href="#comments-form" v-scroll-to="'#comments-form'">Write a Comment</a> */}
              </h2>

              <div name="fade-up" tag="ul" className="comments shadow">
                <li >
                  <div className="comment-author-meta">
                    <div className="avatar">
                      <img src="http://orig07.deviantart.net/4bc8/f/2013/240/2/2/free_avatar_aang_icon_by_zutarart-d6k31hx.gif" alt="comment.user" />
                    </div>
                    <div className="user">Sumair</div>
                    <div className="delete-comment" title="Delete Comment" >&times;</div>
                  </div>
                  <div className="comment-text">
                    <p style={{ whiteSpace: 'pre-line' }} >
                      Lorem ipsum dolor sit amet consectetur, adipisicing elit. Molestiae quae repellendus adipisci in quod, tempore omnis officia minima optio earum ipsa dolor eum magni quam? Perspiciatis soluta esse quibusdam eveniet.
                    </p>
                  </div>
                  <div className="comment-meta">
                    <span className="comment-date">09/09/2019</span><span className="comment-time">18:00</span>
                    <a href="#comments-form" v-scroll-to="'#comments-form'">Reply</a>
                  </div>
                </li>
              </div>


            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
