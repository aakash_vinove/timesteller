// import npm packages
import React, { Component } from 'react';
import {
  Container,
  Card,
  CardHeader,
  CardBody,
} from 'reactstrap';

// import local files
import CustomTable from '../../Common/Table';
import proxy from '../../utils/proxy';
import Loader from '../../Common/Loader/Index';


class Deals extends Component {
  constructor(props) {
    super(props);
    this.state = {
      deals: '',
      showLoader: false,
    };
    this.tableData = this.tableData.bind(this);
    this.getDeals = this.getDeals.bind(this);
  }

  getDeals() {
    this.setState({ showLoader: true });
    if (localStorage.getItem('token')) {
      proxy
        .call('get', `dealList?resturantId=${this.props.id}`, null, null, localStorage.getItem('token'))
        .then((response) => {
          console.log(response, 'hete');
          const deals = [];
          if (
            response &&
            (response.status === 200 || response.status === 201) &&
            response.data &&
            response.data.data

          ) {

            this.setState({
              deals,
              showLoader: false,
            });
          }
        })
        .catch((err) => {
          toasterMessage('error', err.response.data.message);
        });
    }

  }
  tableData() {
    return {
      labels: [{
        label: 'Title',
        value: 'dealTittle',
        isDate: false,
      }, {
        label: 'Description',
        value: 'dealDescription',
        isDate: false,
      }, {
        label: 'Expiry Date',
        value: 'dealExpiryDate',
        isDate: true,
      }, {
        label: 'Expiry Time',
        value: 'dealExpiryTime',
        isDate: false,
      }, {
        label: 'Available Coupon',
        value: 'availableCoupon',
        isDate: false,
      }, {
        label: 'People Limit',
        value: 'peopleLimit',
        isDate: false,
      }, {
        label: 'Deal Type',
        value: 'dealType',
        isDate: false,
      }, {
        label: 'Status',
        value: 'Status',
        isDate: false,
      }],
      // content: deals.map((val) => {
      //   let time = '';
      //   let status = 'active';
      //   if (
      //     val
      //     && val.dealExpiryTime
      //   ) {
      //     time = `${
      //       new Date(val.dealExpiryTime).getHours() > 12 ?
      //         new Date(val.dealExpiryTime).getHours() - 12
      //         :
      //         new Date(val.dealExpiryTime).getHours()
      //     }:${
      //       new Date(val.dealExpiryTime).getMinutes()
      //     } ${
      //       new Date(val.dealExpiryTime).getHours() > 12 ?
      //         'PM'
      //         :
      //         'AM'
      //     }`;
      //   }
      //   if (
      //     val
      //     && val.dealExpiryDate
      //     && val.dealExpiryTime
      //   ) {
      //     if (
      //       new Date(val.dealExpiryDate).setHours(0, 0, 0, 0) < new Date().setHours(0, 0, 0, 0)
      //     ) {
      //       status = 'expired';
      //     } else if (
      //       new Date(val.dealExpiryDate).setHours(0, 0, 0, 0) === new Date().setHours(0, 0, 0, 0)
      //     ) {
      //       if (
      //         new Date(val.dealExpiryTime).getHours() < new Date().getHours()
      //       ) {
      //         status = 'expired';
      //       } else if (
      //         new Date(val.dealExpiryTime).getHours() === new Date().getHours()
      //       ) {
      //         if (new Date(val.dealExpiryTime).getMinutes() < new Date().getMinutes()) {
      //           status = 'expired';
      //         }
      //       }
      //     }
      //   }
      //   return {
      //     ...val,
      //     dealExpiryTime: time,
      //     status,
      //   };
      // }),
    };
  }
  render() {

    return (
      <Container>
        {this.state.showLoader ? <Loader /> : ''}
        <Card>
          <CardHeader>
            Deals
          </CardHeader>
          <CardBody>
            <CustomTable
              data={this.tableData()}
            />
          </CardBody>
        </Card>
      </Container>
    );
  }
}

export default Deals;
