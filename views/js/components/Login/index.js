/*eslint-disable */
import React, { useState, useEffect } from 'react';
import { withRouter} from 'react-router-dom';
import {
  Container,
  Row,
  Col,
  CardGroup,
  Card,
  CardBody,
  Button,
  Input,
  InputGroup,
  InputGroupAddon,
  Form,
} from 'reactstrap';

// import local files
import proxy from '../../utils/proxy';
import { toasterMessage } from '../../utils/toaster';

const Login = (props) => {
  const [formData, setFormData] = useState({
    email: '',
    password: '',
  })
  const { email, password } = formData
  const onChange = e => {
    setFormData({ ...formData, [e.target.name]: e.target.value })
  }
  useEffect(() => {
    if (localStorage.getItem('token')) {
      props.history.push('/admin/dashboard');
    }
  });
  const onSubmit = e => {
    e.preventDefault()

    let userData = {
      email: formData.email,
      password: formData.password
    }
    if (formData.email && formData.password) {
      console.log(formData.email)
      proxy.call('post', 'loginAPI', null, userData)
        .then((response) => {
          console.log(':: response ', response.data.details.accessToken);
          if (response && response.status === 200) {
            toasterMessage('success', 'Login Successful');
            localStorage.setItem('token', response.data.details.accessToken);
            props.history.push('/admin/dashboard');
          }
        })
        .catch((err) => {

          toasterMessage('error', (err.response.data.message ? err.response.data.message : 'Network Error'));
        });
    }
  }
  return (
    <div className="app flex-row align-items-center">
      <Container>
        <Form onSubmit={e => onSubmit(e)}>
          <Row className="justify-content-center">
            <Col md="8">
              <CardGroup className="mb-0">
                <Card className="p-4">
                  <CardBody className="card-body">
                    <h1>Login</h1>
                    <p className="text-muted" />
                    <div>
                      <InputGroup className="mb-3">
                        <InputGroupAddon addonType="prepend"><i className="icon-user" /></InputGroupAddon>
                        <Input
                          type="email"
                          name="email"
                          value={email}
                          onChange={(e) => onChange(e)}
                          placeholder="Email"
                          required
                        />
                      </InputGroup>
                    </div>
                    <div>
                      <InputGroup className="mb-4">
                        <InputGroupAddon addonType="prepend"><i className="icon-lock" /></InputGroupAddon>
                        <Input
                          type="password"
                          name="password"
                          value={password}
                          onChange={(e) => onChange(e)}
                          placeholder="Password"
                          required
                        />
                      </InputGroup>
                    </div>
                    <Row>
                      <Col xs="6">
                        <Button color="primary" type="submit" className="px-4">Login</Button>
                      </Col>
                      <Col xs="6" className="text-right">
                        <Button color="link" className="px-0" onClick={() => { props.history.push('/forgotPassword'); }}>
                          Forgot Password?
                        </Button>
                      </Col>
                    </Row>
                  </CardBody>
                </Card>
              </CardGroup>
            </Col>
          </Row>
        </Form>
      </Container>
    </div>
  );
}
export default withRouter(Login);