/*eslint-disable */
import React, { useState, useEffect } from 'react';
import { BASE_URL } from '../../utils/config'
import {
 Button,
 Row,
 Col,
 FormGroup,
 Form,
 Label,
 Input,
 Modal,
 ModalBody,
 ModalFooter,
 ModalHeader,
 TabContent, TabPane, Nav, NavItem, NavLink,
} from 'reactstrap';
const EditUser=(props)=>{
  console.log("edit props",props)

  console.log("profile Image",props&&props.fields&&props.profileImage)
return(
          <Modal
              isOpen={props.modal}
              toggle={props.toggleEdit}
              className={props.className}
              backdrop={props.backdrop}
            >
              <ModalHeader toggle={props.toggleEdit}>User Edit</ModalHeader>
              <ModalBody>
                <Form onSubmit={props.checkValidations}>
                  <Row className="justify-content-center mb-4">
                    <Col md="4">
                      <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                        <Label for="restaurantName" className="mr-sm-2">
                          title
                        </Label>
                        <div>

                          <Input
                             value={props.editdata.title}
                            type="text"
                            name="firstName"
                            id="firstName"
                            // onChange={props.onChange}
                          />
                        </div>
                        {
                          props.error
                          && props.error.firstName
                          && (<span className="input-error">{props.error.name}</span>)
                        }
                      </FormGroup>
                    </Col>
                    </Row>
                </Form>
              </ModalBody>
              <ModalFooter>
                <Button color="primary"  type="submit" >
                  Save
                </Button>
                &nbsp;
                <Button color="secondary">
                  Cancel
                </Button>
              </ModalFooter>
            </Modal>
)}
export default EditUser