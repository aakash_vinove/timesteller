/*eslint-disable */
import React, { useState, useEffect } from 'react';
import { withRouter } from 'react-router-dom';
import {
  Card, CardImg, CardText, CardBody,
  CardTitle, CardSubtitle,
  Container,
  CardHeader,

  Collapse,
  Button,
  Row,
  Col,
  FormGroup,
  Form,
  Label,
  Input,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  TabContent, TabPane, Nav, NavItem, NavLink
} from 'reactstrap';
import proxy from '../../utils/proxy';
import { toasterMessage } from '../../utils/toaster';
import EditReport from './editReport'
import WatchDetails from './watchDetails'
import axios from 'axios'
import CustomTable from '../../Common/Table';

const ErrorReport = (props) => {

  const [reportList, getReportList] = useState([])
  const [editReportshow, setEditReportShow] = useState(false)
  const [watchDatashow, setWatchDatashow] = useState(false)
  const [watchDetails, setWatchDetails] = useState([])
  const [editdata, setEditData] = useState([])
  const [watchData, setWatchData] = useState({
    watchId: '',
    title: '',
    subTitle: '',
    modelNumber: '',
    price: '',
    summary: '',
    facts: '',
    highlights: '',
    watchCharacteristics: '',
    shortDescription: '',
    type: '',
    idealFor: '',
    style: '',
    status: '',
    manufacturingYear: '',
  })
  const initaildata = () => {
    proxy.call('get', 'reportIncorrectDataAPI', null, null, `Bearer ${localStorage.getItem('token')}`)
      .then((response) => {
        console.log(':: responseErrorReport ', response.data.details);

        if (response && response.status === 200) {
          //toasterMessage('success', 'Login Successful');
          getReportList(response.data.details)

        }
      })
      .catch((err) => {

        toasterMessage('error', (err.response.data.message ? err.response.data.message : 'Network Error'));
      });
  }
  const singleList =(id) =>{
    
    console.log(id)

    proxy.call('get', `reportIncorrectDataAPI/${id}`, null, null, `Bearer ${localStorage.getItem('token')}`)
      .then((response) => {
        console.log(':: responseErrorReport ', response.data.details);

        if (response && response.status === 200) {
          //toasterMessage('success', 'Login Successful');
          

        }
      })
      .catch((err) => {

        toasterMessage('error', (err.response.data.message ? err.response.data.message : 'Network Error'));
      });

  }

  useEffect(() => {
    //getReportList([{ name: "Akash" }, { name: "Akash" }])
    initaildata()
  }, [])



  const tableData = () => {


    console.log("asdadsd", reportList)
    return {
      labels: [
        {
          label: ' User First Name',
          value: "firstName",
          isDate: false,
        },
        {
          label: 'User Last Name',
          value: `lastName`,
          isDate: false,
        },
        {
          label: 'User Email',
          value: `email`,
          isDate: false,
        },
        {
          label: 'Error Title',
          value: 'title',
          isDate: false,
        },
        {
          label: 'Description',
          value: 'description',
          isDate: false,
        },
        {
          label: "status",
          value: "status",
          isDate: false
        },
        {
          label: "Updated",
          value: "updated",
          isDate: false
        },
        {
          label: 'Action',
          value: 'action',
          isDate: false,
        },
      ],
      content: (
        reportList
        && reportList.length
        && reportList.map(customer => ({
          ...customer,
          firstName: (
            customer.userDetail.firstName
          ),
          lastName: (
            customer.userDetail.lastName
          ),
          email: (
            customer.userDetail.email
          ),
          title: (
            customer.errorReport.title
          ),
          description: (
            customer.errorReport.description
          ),
          action: (
            <div style={{ textAlign: 'center' }}>
              <span
                className="action-icon edit-icon"
                onClick={() => { viewwatch(customer) }}
              >
                <i className="icon-pencil" />
              </span>
            </div>

          ),
          status: (
            <div>
              {
                customer.errorReport.isPending?
                <p>Unread</p>
                :
                <p>Read</p>
              }
             
            </div>

          ),
          updated: (
            <div>
              {
                customer.errorReport.isEdited?
                <p>Updated</p>
                :
                <p>Action Pending</p>
              }
             
            </div>

          )
        
        }))
      ) || null,
    };
  }
  const renderAction = (reportList) => {
    return (
      <div style={{ textAlign: 'center' }}>
        <span
          className="action-icon edit-icon"
          onClick={() => { this.edit(reportList); }}
        >
          <i className="icon-pencil" />
        </span>
      </div>
    );
  }
  const viewwatch = (data) => {
    console.log("WatchDetails", data.watchDetail)
    setWatchDatashow(true)
    
    setWatchData({
      ...watchData,
      watchId: data.watchDetail._id,
      title: data.watchDetail.title,
      subTitle: data.watchDetail.subTitle,
      modelNumber: data.watchDetail.modelNumber,
      price: data.watchDetail.price,
      summary: data.watchDetail.summary,
      facts: data.watchDetail.facts,
      highlights: data.watchDetail.highlights,
      watchCharacteristics: data.watchDetail.watchCharacteristics,
      shortDescription: data.watchDetail.shortDescription,
      type: data.watchDetail.type,
      idealFor: data.watchDetail.type,
      style: data.watchDetail.style,
      status: data.watchDetail.status,
      manufacturingYear: data.watchDetail.manufacturingYear,

      reportId: data.errorReport.reportId

    })
    singleList(data.errorReport.reportId)
  }
  const onChange = (e) => {

    console.log(watchData)
    setWatchData({ ...watchData, [e.target.name]: e.target.value })


  }
  const toggle = () => {
    setWatchDatashow(false)
    initaildata()
  }
  const onSubmit = (e) => {
    console.log("after Submit data", watchData)
    let form = new FormData();
    for (var key in watchData) {
      console.log("key>>>", key);
      form.append(key, watchData[key]);
    }
    let token = localStorage.getItem('token')
    if (localStorage.getItem('token')) {


      axios
        .put(`http://timestellarbackend.n1.iworklab.com:3212/editWatchDetailAPI`, form,
          {
            headers: {
              Authorization: "Bearer " + token,
            }
          })
        .then((response) => {
          if (response &&
            (response.status === 200 || response.status === 201) &&
            response.data) {

            toasterMessage(
              'success',
              (
                response
                && response.data

                && response.data.message
              ) || (
                response
                && response.message
              ),
            );

            setWatchDatashow(false)
            initaildata()
            // this.getRestaurantData();
            // this.toggleEdit();
            // this.setState({
            //   ...this.state,

            //   showLoader: false,
            // });

          }
        })
        .catch((err) => {
          alert(err);
          // this.setState({ showLoader: false });
          // toasterMessage(
          //   'error',
          //   (
          //     err
          //     && err.response
          //     && err.response.data
          //     && err.response.data.message
          //   ) || (
          //     err
          //     && err.message
          //   ),
          // );
        });

    }
    // else { this.props.history.push('/'); }


  }
  return (

    <div>

      {
        // reportList && reportList.map((data, index) => {
        //   return (
        //     <Card key={index}>
        //       {/* <CardImg top width="100%" src="/assets/318x180.svg" alt="Card image cap" /> */}
        //       <CardBody>
        //         <CardTitle>Title:-{data && data.errorReport && data.errorReport.title}</CardTitle>
        //         <CardSubtitle>description:-{data && data.errorReport && data.errorReport.description}</CardSubtitle>
        //         {/* <CardText>Some quick example text to build on the card title and make up the bulk of the card's content.</CardText> */}
        //         {/* <Button onClick={() => viewreport(data)}>View User Details</Button> */}
        //         <Button onClick={() => viewwatch(data)}>View watch Details</Button>
        //         {/* <Button onClick={() => viewreport(data)}>Edit</Button> */}
        //       </CardBody>
        //     </Card>


        //)
      }


      <div>

        <div>
          {/* <div className={this.state.showLoader ? 'blurr' : ' '}> */}



          <TabContent activeTab={"1"}>
            <TabPane tabId="1">

              <Row>
                <Col sm="12">
                  <Card>
                    <CardHeader className="bg-dark text-white" >Error Report </CardHeader>
                    <CardBody>

                      <CustomTable data={tableData()} />

                    </CardBody>
                  </Card>
                </Col>

              </Row>
              {/* <Pagination
                activePage={this.state.pagination.activePage}

                totalItemsCount={this.state.pagination.totalItemsCount}
                pageRangeDisplayed={5}
                onChange={this.handlePageChange}
              /> */}

            </TabPane>
            <TabPane tabId="2">


              {/* <Pagination
                activePage={this.state.paginationBlocked.activePage}

                totalItemsCount={this.state.paginationBlocked.totalItemsCount}
                pageRangeDisplayed={5}
                onChange={this.handlePageChange}
              /> */}
            </TabPane>
          </TabContent>
          <Container >
            {/* <AddWatches
              addData={this.addData}
              onChange={this.onChange}
              addModal={this.state.addModal}
              toggleAdd={this.toggleAdd}
              backdrop={this.state.backdrop}
              fields={this.state.fields}
              error={this.state.error}
              className={this.props.className}
              imageShow={this.state.imageShow}
              imageChange={this.imageChange}
              onChangeRating={this.onChangeRating}
            /> */}
            {/* <EditUser 
            modal={this.state.modal}
            toggleEdit={this.toggleEdit}
            className={this.props.className}
            backdrop={this.state.backdrop}
            checkValidations={this.checkValidations}
            fields={this.state.fields}
            onChange={this.onChange}
            
                />
          <DeleteUser 
          delModal={this.state.delModal}
          toggledel={this.toggledel}
          className={this.props.className}
          deleteResturant={this.deleteResturant}
          /> */}
            {/* delete modal starts */}

            {/* delet modal ends */}
            {/* unblock modal starts */}

            {/* unblock modal ends */}


            {/* <Pagination pagination={this.state.pagination} /> */}
          </Container>
        </div>

      </div>

      {/* <EditReport modal={editReportshow} editdata={editdata} /> */}
      <WatchDetails addModal={watchDatashow} watchDetails={watchData} onChange={onChange} onSubmit={onSubmit} toggle={toggle} />

    </div>

  );
};

export default ErrorReport;