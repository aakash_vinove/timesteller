/*eslint-disable */
import React, { useState, useEffect } from 'react';
import {
  Button,
  Row,
  Col,
  FormGroup,
  Form,
  Label,
  Input,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  TabContent, TabPane, Nav, NavItem, NavLink,
} from 'reactstrap';
const AddWatches = (props) => {
  console.log("data in props",props.watchDetails)
  return (
    <Modal
      isOpen={props.addModal}
      toggle={props.toggle}
      className={props.className}
      backdrop={props.backdrop}
    >
      <ModalHeader toggle={props.toggleAdd}>User Watches</ModalHeader>
      <ModalBody>
        <Form onSubmit={props.addData} enctype="multipart/form-data">
          <Row className="justify-content-center mb-4">
            <Col md="4">
              <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                <Label for="restaurantName" className="mr-sm-2">
                  Title
          </Label>
                <div>

                  <Input
                    value={props.watchDetails.title}
                    type="text"
                    name="title"
                    id="title"
                    onChange={props.onChange}
                    
                    
                  />
                </div>
               
              </FormGroup>
            </Col>
            
           
            <Col className='mt-2' md="4">
              <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                <Label for="email" className="mr-sm-2">
                  Model Number
          </Label>
                <Input
                  value={props.watchDetails.modelNumber}
                  type="text"
                  name="modelNumber"
                  id="modelNumber"
                  
                  onChange={props.onChange}
                />
                
              </FormGroup>
            </Col>
            <Col className='mt-2' md="4">
              <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                <Label for="email" className="mr-sm-2">
                  Price($)
          </Label>
                <Input
                  value={props.watchDetails.price}
                  type="number"
                  name="price"
                  id="price"
                  
                  onChange={props.onChange}
                />
               
              </FormGroup>
            </Col>
            <Col className='mt-2' md="4">
              <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                <Label for="email" className="mr-sm-2">
                  Short Description
          </Label>
                <Input
                  value={props.watchDetails.shortDescription}
                  type="text"
                  name="shortDescription"
                  id="shortDescription"
                  
                  onChange={props.onChange}
                />
                
              </FormGroup>
            </Col>
            <Col className='mt-2' md="4">
              <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                <Label for="email" className="mr-sm-2">
                  Sub Title
          </Label>
                <Input
                  value={props.watchDetails.subTitle}
                  type="text"
                  name="subTitle"
                  id="subTitle"
                  
                  onChange={props.onChange}
                />
               
              </FormGroup>
            </Col>
            <Col className='mt-2' md="4">
              <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                <Label for="email" className="mr-sm-2">
                  Summary
          </Label>
                <Input
                  value={props.watchDetails.summary}
                  type="text"
                  name="summary"
                  id="summary"
                  
                  onChange={props.onChange}
                />
               
              </FormGroup>
            </Col>
            <Col className='mt-2' md="4">
              <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                <Label for="email" className="mr-sm-2">
                  Watch Characteristics
          </Label>
                <Input
                  value={props.watchDetails.watchCharacteristics}
                  type="text"
                  name="watchCharacteristics"
                  id="type"
                  
                  onChange={props.onChange}
                />
                
              </FormGroup>
            </Col>
            <Col className='mt-2' md="4">
              <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                <Label for="email" className="mr-sm-2">
                  Ideal For
          </Label>
                <Input
                  value={props.watchDetails.idealFor}
                  type="text"
                  name="idealFor"
                  id="idealFor"
                  
                  onChange={props.onChange}
                />
                
              </FormGroup>
            </Col>
            <Col className='mt-2' md="4">
              <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                <Label for="email" className="mr-sm-2">
                  Style
          </Label>
                <Input
                  value={props.watchDetails.style}
                  type="text"
                  name="style"
                  id="style"
                  
                  onChange={props.onChange}
                />
               
              </FormGroup>
            </Col>
            <Col className='mt-2' md="4">
              <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                <Label for="email" className="mr-sm-2">
                  Status
          </Label>
                <Input
                  value={props.watchDetails.status}
                  type="text"
                  name="status"
                  id="status"
                  
                  onChange={props.onChange}
                />
                
              </FormGroup>
            </Col>
            <Col className='mt-2' md="4">
              <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                <Label for="email" className="mr-sm-2">
                  Manufacturing Year
          </Label>
                <Input
                  value={props.watchDetails.manufacturingYear}
                  type="number"
                  name="manufacturingYear"
                  id="manufacturingYear"
                  
                  onChange={props.onChange}
                />
               
              </FormGroup>
            </Col>
           
            {/* <Col className='mt-2' md="4">
        <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
          <Label for="email" className="mr-sm-2">
            profileImage
          </Label>
          <Input
            type="file"
            name="profileImage"
            id="profileImage"
            onChange={props.imageChange}
          />
         <img id="target" src={props.imageShow} alt="No Image selected" height="200" width="200"/>
        </FormGroup>
      </Col> */}
           
          </Row>
        </Form>
      </ModalBody>
      <ModalFooter>
        <Button color="primary" onClick={props.onSubmit} type="submit" >
          Update
  </Button>
        &nbsp;
  <Button color="secondary" onClick={props.toggle}>
          Cancel
  </Button>
      </ModalFooter>
    </Modal>
  )
}
export default AddWatches