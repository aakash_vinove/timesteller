import React, { Component } from 'react';
import {
  Row, Col, Card, CardHeader, CardBody, FormGroup,
  Form,
  Label,
  Input,
  Button,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Collapse,
} from 'reactstrap';
import Pagination from 'react-js-pagination';
import { toasterMessage } from '../../utils/toaster';
import proxy from '../../utils/proxy';
import Loader from '../../Common/Loader/Index';
import CustomTable from '../../Common/Table';

import Data from '../Deals/dummy.json';

export default class index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showLoader: false,
      data: '',
      heading: 'Add New',
      dealName: '',
      filter: '',
      id: '',
      error: {},
      delModal: false,
      pagination: {
        activePage: 1,
        itemsCountPerPage: 10,
        pageRangeDisplayed: 5,
      },

    };
    this.toggle = this.toggle.bind(this);
    this.toggledel = this.toggledel.bind(this);
    this.deletDealType = this.deletDealType.bind(this);
    this.handlePageChange = this.handlePageChange.bind(this);
    this.exportDealType = this.exportDealType.bind(this);
  }
  componentDidMount() {
    this.getDeals();
  }


  getDeals() {
    if (localStorage.getItem('token')) {
      const currentPage = this.state.pagination.activePage;
      const currentPag = { currentPage };
      this.setState({ showLoader: true });

      proxy.call('get', 'dealType', currentPag, null, localStorage.getItem('token'))
        .then((response) => {

          if (response &&
            (response.status === 200 || response.status === 201) &&
            response.data &&
            response.data
          ) {
            this.setState({
              ...this.state,
              data: response.data.data,
              showLoader: false,

              pagination: {
                ...this.state.pagination,
                totalItemsCount: response.data.pageCount,
              },
            });

          }
        })
        .catch((err) => {
          // this.setState({ showLoader: false });
          toasterMessage(
            'error',
            (
              err
              && err.response
              && err.response.data
              && err.response.data.message
            ) || (
              err
              && err.message
            ),
          );
        });

    } else { this.props.history.push('/'); }
  }
  edit(item) {
    this.setState({ heading: 'Edit', dealName: item.dealType, id: item._id });

  }
  toggle() {
    this.setState(state => ({ collapse: !state.collapse }));
  }
  toggledel() {
    this.setState(prevState => ({
      delModal: !prevState.delModal,
    }));
  }
  remove(item) {
    this.setState({ id: item._id });
    this.toggledel();
  }
  applyFilters(e) {
    e.preventDefault();
    if (this.state.filter === '') {
      alert('enter some text to filter');
    } else {


      this.setState({ showLoader: true });

      if (localStorage.getItem('token')) {
        proxy
          .call('get', `filterDealType/${this.state.filter}`, null, null, localStorage.getItem('token'))
          .then((response) => {
            console.log(response, 'filtered');
            if (
              response
              && (
                response.status === 200
                || response.status === 201
              )
            ) {
              console.log(response.data.pageCount);
              this.setState({
                ...this.state,
                data: response.data.data,

                showLoader: false,

                pagination: {
                  ...this.state.pagination,
                  totalItemsCount: response.data.pageCount,
                },
              });
            }
          })
          .catch((err) => {
            // this.setState({ showLoader: false });
            toasterMessage(
              'error',
              (
                err
                && err.response
                && err.response.data
                && err.response.data.message
              ) || (
                err
                && err.message
              ),
            );
          });
      } else {
        this.state.showLoader = true;
        this.props.history.push('/');
      }
    }

    console.log(this.state.filter, 'filterrr');
  }
  clearFilters(e) {

    e.preventDefault();
    this.getDeals();
    this.setState({ filter: '' });
  }
  deletDealType() {

    const id = this.state.id;
    const Id = { id };
    if (localStorage.getItem('token')) {
      this.setState({ showLoader: true });
      proxy.call('delete', 'dealTypeDelete', Id, null, localStorage.getItem('token'))
        .then((response) => {

          if (
            response
            && (
              response.status === 200
              || response.status === 201
            )
            && response.data


          ) {
            toasterMessage(
              'success',
              (
                response
                && response.data

                && response.data.message
              ) || (
                response
                && response.message
              ),
            );
            this.getDeals();
            this.toggledel();
            this.setState({
              ...this.state,

              showLoader: false,

            });
          }
        })
        .catch((err) => {
          toasterMessage(
            'error',
            (
              err
              && err.response
              && err.response.data

            ) || (
              err
              && err.message
            ),
          );
        });

    } else {

      this.props.history.push('/');
    }
  }
  checkValidations(e) {
    e.preventDefault();
    const { dealName, error } = this.state;
    if (!dealName) {
      error.dealName = 'Title Required';
    } else if (dealName.length < 2) { error.dealName = 'Title should be  3 char long'; } else {
      delete error.dealName;
      this.Save();
    }
    this.setState({
      error,
    });
  }
  Save() {

    const id = this.state.id;
    const Id = { id };
    const body = {
      dealType: this.state.dealName,
    };


    if (this.state.heading === 'Add New') {
      this.setState({ showLoader: true });
      if (localStorage.getItem('token')) {

        proxy.call('post', 'dealType', null, body, localStorage.getItem('token'))
          .then((response) => {

            if (response &&
              (response.status === 200 || response.status === 201)
            ) {

              this.getDeals();
              toasterMessage(
                'success',
                (
                  response
                  && response.data

                  && response.data.message
                ) || (
                  response
                  && response.message
                ),
              );
              this.setState({ dealName: '', heading: 'Add New', showLoader: false });
            }
          })
          .catch((err) => {
            // this.setState({ showLoader: false });
            toasterMessage(
              'error',
              (
                err
                && err.response
                && err.response.data
                && err.response.data.message
              ) || (
                err
                && err.message
              ),
            );
          });
      }

    } else if (this.state.heading === 'Edit' && localStorage.getItem('token')) {
      this.setState({ showLoader: true });
      proxy.call('put', 'dealTypeUpdate', Id, body, localStorage.getItem('token'))
        .then((response) => {
          if (response &&
            (response.status === 200 || response.status === 201)) {
            this.getDeals();
            toasterMessage(
              'success',
              (
                response
                && response.data

                && response.data.message
              ) || (
                response
                && response.message
              ),
            );
            this.setState({ dealName: '', heading: 'Add New', showLoader: false });
          }
        })
        .catch((err) => {
          toasterMessage(
            'error',
            (
              err
              && err.response
              && err.response.data
              && err.response.data.message
            ) || (
              err
              && err.message
            ),
          );
        });

    } else {
      this.props.history.push('/');
    }

  }
  handlePageChange(pageNumber) {

    this.setState({
      ...this.state, pagination: { activePage: pageNumber },
    }, () => this.getDeals());
  }
  exportDealType() {
    if (localStorage.getItem('token')) {
      proxy
        .call('get', 'exportToExcelDealType', null, null, localStorage.getItem('token'))
        .then((res) => {
          const url = 'http://realtimedeals.n1.iworklab.com:3005/v1/resturant/exportToExcelDealType';
          window.open(url, '_blank')
            .focus();
        });
    }
  }
  tableData() {
    return {
      labels: [{
        label: 'Title',
        value: 'dealType',
        isDate: false,
      }, {
        label: 'Created At',
        value: 'createdAt',
        isDate: true,
      }, {
        label: 'Updated At',
        value: 'updatedAt',
        isDate: true,
      }, {
        label: 'Action',
        value: 'action',
        isDate: false,
      }],
      content: (
        this.state.data
        && this.state
        && this.state.data.map(deal => ({
          ...deal,
          action: (
            <div style={{ textAlign: 'center' }}>
              <span
                className="action-icon edit-icon"
                onClick={() => { this.edit(deal); }}
              >
                <i className="icon-pencil" />
              </span>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <span
                className="action-icon remove-icon"
                onClick={() => { this.remove(deal); }}
              >
                <i className="icon-trash" />
              </span>
            </div>
          ),
        }))
      ) || null,
    };
  }

  renderFilter() {
    return (
      <Form>
        <Row className="justify-content-center mb-4">
          <Col md="6">
            <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
              <Label for="restaurantName" className="mr-sm-2">
                Search
              </Label>
              <Input
                value={this.state.filter}
                type="text"
                placeholder="Search By Title"
                name="resturantName"
                id="restaurantName"
                onChange={(event) => {
                  this.setState({ filter: event.target.value });
                }}
              />
            </FormGroup>
          </Col>


        </Row>
        <Row className="justify-content-center ">
          <Col md="6">

            <Button onClick={(e) => { this.applyFilters(e); }} color="primary" type="submit">
              Apply Filters
            </Button>
          </Col>
          <Col md="3">
            <Button onClick={(e) => { this.clearFilters(e); }} color="secondary button-clear">Clear</Button>
          </Col>
        </Row>

      </Form>
    );
  }

  render() {
    return (
      <div className="container-fluid p-4" >

        {/* delete modal starts */}
        <Modal
          style={{ width: '25rem' }}
          isOpen={this.state.delModal}
          toggle={this.toggledel}
          className={this.props.className}
        >
          <ModalHeader toggle={this.toggledel}>Delete</ModalHeader>
          <ModalBody>Are you sure want to Delete ?  Once deleted can't be retreived </ModalBody>
          <ModalFooter>
            <Button color="danger" onClick={this.deletDealType}>
              Delete
            </Button>
            <Button color="secondary" onClick={this.toggledel}>
              Cancel
            </Button>
          </ModalFooter>
        </Modal>
        {/* delet modal ends */}

        {this.state.showLoader ? <Loader /> : ''}
        <div className={this.state.showLoader ? 'blurr' : ' '}>
          <Row>
            <Col xs="6" >
              <Card>
                <CardHeader className="bg-dark text-white">{this.state.heading}</CardHeader>
                <CardBody >
                  <Form>
                    <Row className="justify-content-center mb-4">
                      <Col md="6">
                        <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                          <Label for="restaurantName" className="mr-sm-2">
                            Deal type
                          </Label>

                          <Input
                            value={this.state.dealName}
                            type="text"
                            name="resturantName"
                            id="restaurantName"
                            onChange={(event) => {
                              this.setState({ dealName: event.target.value });
                            }}
                          />
                          {
                            this.state.error
                            && this.state.error.dealName
                            && (<span className="input-error">{this.state.error.dealName}</span>)
                          }
                        </FormGroup>
                      </Col>


                    </Row>
                    <Row className="justify-content-center" >
                      <Col md="3" >
                        <Button onClick={(e) => { this.checkValidations(e); }} color="primary" type="submit">
                          Save
                        </Button>
                      </Col>
                      <Col md="3"><Button color="secondary button-clear">Clear</Button></Col>


                    </Row>

                  </Form>
                </CardBody>
              </Card>
              {/* <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Ducimus porro architecto voluptate soluta sunt necessitatibus, consequatur veniam? Optio minus, veritatis aut, fugiat dignissimos vel provident similique iure corporis recusandae ullam?</p> */}
            </Col>
            <Col xs="6" className="" >

              <Card>
                <CardHeader className="bg-dark text-white" >All deals</CardHeader>
                <CardBody>
                  <Row>
                    <Col md="12">
                      <Button onClick={this.exportDealType} >Export</Button>
                      <Button
                        color="primary"
                        onClick={this.toggle}
                        style={{ marginBottom: '1rem', float: 'right' }}
                      >
                        Filter
                      </Button>
                    </Col>
                  </Row>
                  <Collapse className="collapse-filter" isOpen={this.state.collapse}>
                    <Card className="filter-container">
                      {this.renderFilter()}
                    </Card>
                  </Collapse>
                  <CustomTable data={this.tableData()} />
                </CardBody>
              </Card>

              <Pagination
                activePage={this.state.pagination.activePage}

                totalItemsCount={this.state.pagination.totalItemsCount}
                pageRangeDisplayed={5}
                onChange={this.handlePageChange}
              />
            </Col>
          </Row>
        </div>

      </div>

    );
  }
}
