/*eslint-disable */
import React, { useState, useEffect } from 'react';
import { withRouter} from 'react-router-dom';
import {
  Container,
  Row,
  Col,
  CardGroup,
  Card,
  CardBody,
  Button,
  Input,
  InputGroup,
  InputGroupAddon,
  Form,
} from 'reactstrap';

// import local files
import proxy from '../../utils/proxy';
import { toasterMessage } from '../../utils/toaster';

const ResetPassword = (props) => {
  const [formData, setFormData] = useState({
    oldPassword: '',
    newPassword: '',
    confirmPassword:''
  })
  const { oldPassword, newPassword ,confirmPassword} = formData
  const onChange = e => {
    setFormData({ ...formData, [e.target.name]: e.target.value })
  }
 
  const onSubmit = e => {
    e.preventDefault()
    let userData = {
     oldPassword: formData.oldPassword,
     newPassword: formData.newPassword,
     confirmPassword:formData.confirmPassword
    }
    if (formData.oldPassword && formData.newPassword && formData.confirmPassword) {
     
      proxy.call('post', 'passwordResetAPI', null, userData,'Bearer '+localStorage.getItem('token'))
        .then((response) => {
          console.log(':: response ', response);
          if (response && response.status === 201) {
            toasterMessage('success', response.data.message);
           
            props.history.push('/admin/dashboard');
          }
        })
        .catch((err) => {

          toasterMessage('error', (err.response.data.message ? err.response.data.message : 'Network Error'));
        });
    }
  }
  return (
    <div className="app flex-row align-items-center">
      <Container>
        <Form onSubmit={e => onSubmit(e)}>
          <Row className="justify-content-center">
            <Col md="8">
              <CardGroup className="mb-0">
                <Card className="p-4">
                  <CardBody className="card-body">
                    <h1>Login</h1>
                    <p className="text-muted" />
                    <div>
                      <InputGroup className="mb-3">
                        <InputGroupAddon addonType="prepend"><i className="icon-user" /></InputGroupAddon>
                        <Input
                          type="password"
                          name="oldPassword"
                          value={oldPassword}
                          onChange={(e) => onChange(e)}
                          placeholder="old Password"
                          required
                        />
                      </InputGroup>
                    </div>
                    <div>
                      <InputGroup className="mb-4">
                        <InputGroupAddon addonType="prepend"><i className="icon-lock" /></InputGroupAddon>
                        <Input
                          type="password"
                          name="newPassword"
                          value={newPassword}
                          onChange={(e) => onChange(e)}
                          placeholder="New Password"
                          required
                        />
                      </InputGroup>
                    </div>
                    <div>
                      <InputGroup className="mb-4">
                        <InputGroupAddon addonType="prepend"><i className="icon-lock" /></InputGroupAddon>
                        <Input
                          type="password"
                          name="confirmPassword"
                          value={confirmPassword}
                          onChange={(e) => onChange(e)}
                          placeholder="Confirm Password"
                          required
                        />
                      </InputGroup>
                    </div>
                    <Row>
                      <Col xs="6">
                        <Button color="primary" type="submit" className="px-4">Submit</Button>
                      </Col>
                      
                    </Row>
                  </CardBody>
                </Card>
              </CardGroup>
            </Col>
          </Row>
        </Form>
      </Container>
    </div>
  );
}
export default withRouter(ResetPassword);