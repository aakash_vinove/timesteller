import React from 'react';
import { render } from 'react-dom';

import Routes from '../../routes/Routes';
import '../css/developer.css';
import { initializeFirebase } from './push-notification';


render(
  <Routes />,
  document.getElementById('root'),
);
initializeFirebase();
