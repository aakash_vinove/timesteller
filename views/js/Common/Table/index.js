// import npm packages
import React, { Component } from 'react';
import { Table } from 'reactstrap';

class CustomTable extends Component {
  render() {
    return (
      <Table hover className="table-data">
        <thead>
          <tr>
            {
              this.props.data
              && this.props.data.labels
              && this.props.data.labels.length
              && this.props.data.labels.map((header, index) => (
                <th key={`header-${index}`}>{header.label}</th>
              ))
            }
          </tr>
        </thead>
        <tbody>
          {
            (
              this.props.data
              && this.props.data.content
              && this.props.data.content.length
              && this.props.data.content.map(data => (
                <tr key={data._id}>
                  {
                    this.props.data
                    && this.props.data.labels
                    && this.props.data.labels.length
                    && this.props.data.labels.map((header, index) => (
                      <td key={`cell-${index}`}>
                        {
                          (header.isDate && new Date(data[header.value]).toDateString()) || data[header.value]
                        }
                      </td>
                    ))
                  }
                </tr>
              ))
            ) || (
              this.props.data.content
              && this.props.data.content.length === 0
              && (
                <tr>
                  <td
                    colSpan={this.props.data.labels && this.props.data.labels.length || 0}
                    style={{ textAlign: 'center' }}
                  >
                    No Record Found
                  </td>
                </tr>
              )
            ) || (<tr><td /></tr>)
          }
        </tbody>
      </Table>
    );
  }
}
export default CustomTable;
