import React, { Component } from 'react';
import Pagination from 'react-js-pagination';


export default class index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activePage: 15,
    };
  }
  handlePageChange(pageNumber) {
    console.log(`active page is ${pageNumber}`);
    // this.setState({ activePage: pageNumber });
  }
  render() {
    console.log(this.props);
    return (
      <div >
        <Pagination
          activePage={1}
          itemsCountPerPage={10}
          totalItemsCount={10}
          pageRangeDisplayed={5}
          onChange={this.handlePageChange}
        />
      </div>
    );
  }
}
