/*eslint-disable */
// api base url
export const BASE_URL1 = 'http://realtimedeals.n1.iworklab.com:3005/v1/resturant/';
export const BASE_URL = 'http://timestellarbackend.n1.iworklab.com:3212/';
// export const BASE_URL = 'http://10.0.2.219:5500/v1/resturant/';

// passowrd regex
export const passwordRegex = /^(?=.*\d)(?=.*[!"#$%&'()*+,-./:;<=>?@[\]^_`{|}~])(?=.*[a-z])(?=.*[A-Z])[a-zA-Z0-9!"#$%&'()*+,-./:;<=>?@[\]^_`{|}~]{8,}$/;

// email regex
export const emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

// mobile regex
// export const mobileRegex = /^([0|\+[0-9]{1,5})?([7-9][0-9]{9})$/;
export const mobileRegex = /^[0-9]{8,14}$/;
