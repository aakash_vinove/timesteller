const path = require("path");
const webpack = require("webpack");
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const Visualizer = require('webpack-visualizer-plugin');

module.exports = {
	cache: true,
	entry: path.join(__dirname, "views/js/app.js"),
	output: {
		path: path.join(__dirname, "public/javascripts"),
		publicPath: "/public/javascripts/",
		filename: "bundle.js"
	},
	module: {
		loaders: [
      {
        test: /\.js(x)?$/,
        loader: "babel-loader",
				exclude: /node_modules/,
        query: {
          presets: ["env", "react"],
          plugins: ["transform-decorators"]
        }
      },
			{
        test: /\.css$/,
        use: [
          {
            loader: 'style-loader'
          },
          {
            loader: 'css-loader'
          },
          {
            loader: 'postcss-loader',
            options: {
              plugins: () => [require('autoprefixer')]
            }
          }
        ]
}
    ]
	},
	plugins: [
		new UglifyJSPlugin({
			sourceMap: true
		}),
		new webpack.DefinePlugin({
			'process.env.NODE_ENV': JSON.stringify('production')
		}),
		new Visualizer()
	]
};
